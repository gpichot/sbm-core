/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */


#ifndef EXTRA_BENCHMARK_H
#define EXTRA_BENCHMARK_H
#include "sbm.h"
#include "assert.h"
#include "sbm_error_code.h"
#include "api_json_jansson.h"
#include "api_config.h"

/** \brief Common methods for bimaterial benchmarks tests.
 *
 * @file extra_benchmark.h
 *
 * \author Antoine Lejay
 * \author Géraldine Pichot
 */

/** \addtogroup extra_benchmark */
/** @{ */


extern const double SQRT3; //=1.73205080756888;

/*** \brief Structure for the position (in time and space) of the particle. */
typedef struct {
    /** @brief Position */
    double xpos;
    /** @brief Time */
    double time;
    /** @brief Alive? */
    int alive;
} particle;

/** \brief Structure to store data related to one interface. */
typedef struct {
    /** \brief Structure for the media.*/
    media *media;
    /** \brief Time step and its square root.*/
    time_struct *time;
    /** \brief Position of the left-end limit of the interface layer.*/
    double left_layer;
    /** \brief Position of the right-end limit of the interface layer.*/
    double right_layer;
    /** \brief Position of the interface */
    double position;
} onestep_disc_struct;

/** \brief Structure for defining the media and encoding its geometry. */
typedef struct {
    /** \brief Diffusion coefficient in the left part. */
    double D0;
    /** \brief Square root of the diffusion coefficient in the left part. */
    double sqrtD0;
    /** \brief Diffusion coefficient in the right part. */
    double D1;
    /** \brief Square roof of the diffusion coefficient in the right part. */
    double sqrtD1;
    /** \brief Time step for the particle. */
    double deltat;
    /** \brief Structure for the time step of the particle. */
    time_struct *time;
    /** \brief Lenght of the left and right media.*/
    double L0,L1;
    /** \brief Structure for dealing with the interfaces. */
    onestep_disc_struct *os1;
    /** \brief Total length of the cell */
    double length;
    /** \brief Limit for the left absorbing condition */
    double limit_layer_left;
    /** \brief Limit for the left absorbing condition */
    double limit_layer_right;
} struct_media;

/** @brief Possibilities for the starting position */
enum StartingPosition {FIXED_SP,UNIFORM_SP};

/** @brief Structure for the coefficients.
 *
 * The initialization methods are defined in each benchmark as
 * they could be different.
 * */
typedef struct {
    /** @brief Parameters contained in the file and that are only used in the initialization step. */
    json_obj *obj;
    /** @brief Final time */
    double final_time;
    /** @brief Time for sampling */
    double sample_time;
    /** @brief Time step */
    double deltat;
    /** @brief Number of particles */
    size_t nb;
    /** @brief Starting point */
    double starting_point;
    /** @brief Type of starting */
    enum StartingPosition starting;
    /** @brief The method to deal with the interface */
    double (*method)(api_rng *, media *, time_struct *, double );
    /** @brief The method outside the interface layer*/
    double (*method_outlayer)(api_rng *, double, time_struct *, double ); 
    /** @brief The method for the left boundary */
    void (*method_left_boundary)(api_rng *, struct_media *,particle *);
    /** @brief The method for the right boundary */
    void (*method_right_boundary)(api_rng *, struct_media *,particle *);
    /** @brief Name of the directory of the current simulation. */
    char prefix[50];
    /** @brief Suffix for files */
    char suffix[50];
    /** @brief Directory where the output files are written. */
    char directory[512];
} coefficients;



void onestep_left_absorbing_boundary_layer_normal_exact(api_rng *rng, struct_media *med, particle *part);
void onestep_left_absorbing_boundary_layer_uniform_linear(api_rng *rng, struct_media *med, particle *part);
void onestep_left_absorbing_boundary_layer_normal_linear(api_rng *rng, struct_media *med, particle *part);

void onestep_right_absorbing_boundary_layer_normal_exact(api_rng *rng, struct_media *med, particle *part);
void onestep_right_absorbing_boundary_layer_uniform_linear(api_rng *rng, struct_media *med, particle *part);
void onestep_right_absorbing_boundary_layer_normal_linear(api_rng *rng, struct_media *med, particle *part);

struct_media *struct_media_init(coefficients *coeff);
json_obj *struct_media_to_json(struct_media *med);


int onestep_disc_struct_is_in_layer(onestep_disc_struct *os,double pos);
onestep_disc_struct *onestep_disc_struct_init(double d1, double d2, double position, double deltat);
void onestep_disc_struct_next(api_rng *rng,onestep_disc_struct *os,double *pos,double *time,double (*ds)(api_rng *, media *, time_struct *, double ));

#endif

/** @} */
