/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

#include "extra_benchmark.h"

/** \brief Common methods for bimaterial benchmarks tests.
 *
 * @file extra_benchmark.c
 *
 * @author Antoine Lejay
 * @author Géraldine Pichot
 *
 * @date 2019-01-03
 * @version 1.0
 */

/** \addtogroup extra_benchmark */
/** @{ */

extern const double SQRT3; //=1.73205080756888;

/** @brief One step for the particle with absorption on the left (at 0) with the exact distribution of the hitting time.
 *
 * @param[in,out] rng Random number generator
 * @param[in] med The media, only the diffusion coefficient \p med->sqrtD0 on the left is used.
 * @param[in,out] part The particle (updated).
 * 
 * @return Void.
 */
void onestep_left_absorbing_boundary_layer_normal_exact(api_rng *rng, struct_media *med, particle *part)
{
    double time;
    double npos;
    int hit;
    r_pos_bm_hit_or_continue(rng,med->time->time,med->time->sqrt_time,part->xpos/med->sqrtD0,&time,&npos,&hit);
    if(hit)
    {
	part->xpos=0.0;
	part->time+=time;
	part->alive=0;
	return;
    }
    else
    {
	part->xpos=npos*med->sqrtD0;
	part->time+=time;
	part->alive=1;
	return;
    }
}

/** @brief One step for the particle with absorption on the left (at 0) with a uniform 
 * distribution of the position and a linear interpolation of the hitting time.
 *
 * @param[in,out] rng Random number generator
 * @param[in] med The media, only the diffusion coefficient \p med->sqrtD0 on the left is used.
 * @param[in,out] part The particle (updated).
 *
 * @return Void.
 */
void onestep_left_absorbing_boundary_layer_uniform_linear(api_rng *rng, struct_media *med, particle *part)
{
    double time;
    double npos;
    npos=part->xpos+SQRT3*med->time->sqrt_time*med->sqrtD0*(2*api_rng_uniform(rng)-1);
    time=med->time->time;
    if(npos<0.0)
    {
	part->xpos=0.0;
	part->time+=time*part->xpos/(part->xpos-npos);
	part->alive=0;
	return;
    }
    else
    {
	part->xpos=npos;
	part->time+=time;
	part->alive=1;
	return;
    }
}

/** @brief One step for the particle with absorption on the left (at 0) with a Gaussian step 
 * and a linear interpolation of the hitting time.
 *
 * @param rng Random number generator
 * @param med The media, only the diffusion coefficient \p med->sqrtD0 on the left is used.
 * @param part The particle.
 * 
 * @return Void.
 */
void onestep_left_absorbing_boundary_layer_normal_linear(api_rng *rng, struct_media *med, particle *part)
{
    double time;
    double npos;
    npos=part->xpos+med->time->sqrt_time*med->sqrtD0*api_rng_gaussian(rng,1.0);
    time=med->time->time;
    if(npos<0.0)
    {
	part->xpos=0.0;
	part->time+=time*part->xpos/(part->xpos-npos);
	part->alive=0;
	return;
    }
    else
    {
	part->xpos=npos;
	part->time+=time;
	part->alive=1;
	return;
    }
}


/** @brief One step for the particle with absorption on the right (at 0) with a exact distribution
 * of the hitting time.
 *
 * @param rng Random number generator.
 * @param med The media, only the diffusion coefficient \p med->sqrtD1 on the right is used.
 * @param part The particle.
 * 
 * @return Void. 
 */
void onestep_right_absorbing_boundary_layer_normal_exact(api_rng *rng, struct_media *med, particle *part)
{
    double time;
    double npos;
    int hit;
    r_pos_bm_hit_or_continue(rng,med->time->time,med->time->sqrt_time,(part->xpos-med->length)/med->sqrtD1,&time,&npos,&hit);
    if(hit)
    {
	part->xpos=med->length;
	part->time+=time;
	part->alive=0;
	return;
    }
    else
    {
	part->xpos=med->length-npos*med->sqrtD1;
	part->time+=time;
	part->alive=1;
	return;
    }
}

/** @brief One step for the particle with absorption on the right (at 0) with a uniform step 
 * and a linear interpolation of the hitting time.
 *
 * @param [in,out] rng Random number generator.
 * @param [in] med The media, only the diffusion coefficient \p med->sqrtD1 on the right is used.
 * @param [in,out] part Time and position of the particle, updated.
 * 
 * @return Void. 
 */
void onestep_right_absorbing_boundary_layer_uniform_linear(api_rng *rng, struct_media *med, particle *part)
{
    double time;
    double npos;
    npos=part->xpos+SQRT3*med->time->sqrt_time*med->sqrtD1*(2*api_rng_uniform(rng)-1);
    time=med->time->time;
    if(npos>med->length)
    {
	part->xpos=med->length;
	part->time+=time*(med->length-part->xpos)/(npos-part->xpos);
	part->alive=0;
	return;
    }
    else
    {
	part->xpos=npos;
	part->time+=time;
	part->alive=1;
	return;
    }
}

/** @brief One step for the particle with absorption on the right (at 0) with a Gaussian step 
 * and a linear interpolation of the hitting time.
 *
 * @param [in,out] rng Random number generator.
 * @param [in] med The media, only the diffusion coefficient \p med->sqrtD1 on the right is used.
 * @param [in,out] part Time and position of the particle, updated.
 *
 * @return Void.
 */
void onestep_right_absorbing_boundary_layer_normal_linear(api_rng *rng, struct_media *med, particle *part)
{
    double time;
    double npos;
    npos=part->xpos+med->time->sqrt_time*med->sqrtD1*api_rng_gaussian(rng,1.0);
    time=med->time->time;
    if(npos>med->length)
    {
	part->xpos=med->length;
	part->time+=time*(med->length-part->xpos)/(npos-part->xpos);
	part->alive=0;
	return;
    }
    else
    {
	part->xpos=npos;
	part->time+=time;
	part->alive=1;
	return;
    }
}



/** @brief Tranform the media into a JSON object. 
 *
 * @param med The media.
 *
 * @return A JSON object.
 * */
json_obj *struct_media_to_json(struct_media *med)
{
    json_obj *obj=json_obj_init(); 
    json_obj_add_string(obj,"type","bimaterial");
    json_obj_add_double(obj,"D0",0.5*med->D0);
    json_obj_add_double(obj,"D1",0.5*med->D1);
    json_obj_add_double(obj,"L0",med->L0);
    json_obj_add_double(obj,"L1",med->L1);
    json_obj *layers=json_arr_init();
    json_arr_push_double(layers,0.0);
    json_arr_push_double(layers,med->limit_layer_left);
    json_arr_push_double(layers,med->os1->left_layer);
    json_arr_push_double(layers,med->os1->position);
    json_arr_push_double(layers,med->os1->right_layer);
    json_arr_push_double(layers,med->limit_layer_right);
    json_arr_push_double(layers,med->length);
    json_obj_add_obj(obj,"layers",layers);
    json_obj_add_double(obj,"length",med->length);
    json_obj_add_double(obj,"interface",med->L0);
    return(obj);
}

/** @brief Change the position of a process generated by a divergence form operator with a given algorithm around an interface. 
 *
 * The function is passed as an argument, which assumed that there
 * is one discontinuity at position 0. Ths time is incremented, and the position is shifted.
 *
 * @param[in,out] rng Random Number Generator.
 * @param[in] os Structure containing the interface.
 * @param[in,out] pos Pointer to the position, updated.
 * @param[in,out] time Point to the time, updated.
 * @param ds Function to perform one step with a discontinuity at 0.
 *
 * @return Void.
 */
void onestep_disc_struct_next(api_rng *rng,onestep_disc_struct *os,double *pos,double *time,double (*ds)(api_rng *, media *, time_struct *, double ))
{
    (*time)+=os->time->time;
    (*pos)=(*ds)(rng,os->media,os->time,(*pos)-os->position)+os->position;
    return;
}

/** @brief Initialize a onestep structure.
 *
 * @param[in] d1 Diffusion coefficient on the left of the interface.
 * @param[in] d2 Diffusion coefficient on the right of the interface.
 * @param[in] position Position of the interface.
 * @param[in] deltat Time step.
 *
 * @return A new structure gathering data about interfaces.
 */
onestep_disc_struct *onestep_disc_struct_init(double d1, double d2, double position, double deltat) 
{
    onestep_disc_struct *os;
    os=malloc(sizeof(onestep_disc_struct));
    os->media=media_init(d1,d2,LAYER_FACTOR);
    os->time=time_struct_init(deltat);
    os->left_layer=position-os->time->sqrt_time*LAYER_FACTOR*os->media->sqrtd1;
    os->right_layer=position+os->time->sqrt_time*LAYER_FACTOR*os->media->sqrtd2;
    os->position=position;
    return(os);
}

/** @brief Check if the position is in the layer. 
 *
 * @param[in] os Structure for one interface. 
 * @param[in] pos A position. 
 *
 * @return 1 or 0 whether the position is in the interface layer or not.
 * */
int onestep_disc_struct_is_in_layer(onestep_disc_struct *os,double pos)
{
    return((pos>=os->left_layer) && (pos<=os->right_layer));
}



/** @brief Initialize a media structure from the coefficients.
 *
 * @warning Simulate the process generated by the operator \f$\nabla(D\nabla\cdot) \f$
 * so that the diffusion coefficients D0 and D1 are multiplied by 2.
 *
 * @param[in,out] coeff The parameters. 
 *
 * @return A media structure.
 *
 * The media characteristics are stored as JSON key/values pair in \p coeff->obj. These data are used.
 */
struct_media *struct_media_init(coefficients *coeff)
{
    struct_media *med;
    med=malloc(sizeof(struct_media));
    // Simulation with the one-half factor
    med->D0=2*json_obj_read_double(coeff->obj,"D0");
    assert(med->D0>0.0); // if the key is not defined, then med->D0 is set to 0.0
    med->sqrtD0=sqrt(med->D0);
    med->D1=2*json_obj_read_double(coeff->obj,"D1");
    assert(med->D1>0.0); // if the key is not defined, then med->D1 is set to 0.0
    med->sqrtD1=sqrt(med->D1);
    med->deltat=coeff->deltat;
    med->time=time_struct_init(coeff->deltat);
    med->L0=json_obj_read_double(coeff->obj,"L0");
    assert(med->L0>0.0);
    med->L1=json_obj_read_double(coeff->obj,"L1");
    assert(med->L1>0.0);
    med->length=med->L0+med->L1;
    med->os1=onestep_disc_struct_init(med->D0,med->D1,med->L0,coeff->deltat);
    med->limit_layer_left=(LAYER_FACTOR)*med->sqrtD0*sqrt(coeff->deltat);
    med->limit_layer_right=med->length-(LAYER_FACTOR)*med->sqrtD1*sqrt(coeff->deltat);
    if(med->os1->left_layer<med->limit_layer_left)
    {
	fprintf(stderr,"The left layer is at %f and is below the boundary layer at %f\n",med->os1->left_layer,med->limit_layer_left);
	exit(ERROR_IMPROPER_LAYERS_CONFIGURATION);
    }
    if(med->os1->right_layer>med->limit_layer_right)
    {
	fprintf(stderr,"The left layer is at %f and is bigger than the right boundary layer at %f\n",med->os1->right_layer,med->limit_layer_right);
	exit(ERROR_IMPROPER_LAYERS_CONFIGURATION);
    }
    json_obj *obj_media=struct_media_to_json(med);
    json_obj_add_obj(coeff->obj,"media",obj_media);
    return(med);
}

/** @} */
