GOAL
====

This set of tools aims at performing benchmark tests for Monte Carlo methods
for moving particles in one-dimensional discontinuous media. Some part
of libraries, such as sbm.c/sbm.h may serve for performing other simulations. 

LANGUAGES
=========

The core library is written in `C`. The [JSON](http://json.org) format is used for configuration files and some of the
output files. It could be dealt with a lot of tools.

HOW TO CITE?
============

A. Lejay and G. Pichot, Simulating Diffusion Processes in Discontinuous Media: Benchmark Tests, 
Journal of Computational Physics, 314, 348-413 (2016). DOI:10.1016/j.jcp.2016.03.003

LICENCE
=======

This work is licenced under the LGPL: See Licence file.


DEPENDENCIES AND EXTERNAL LIBRARIES
===================================

The [Gnu Scientific Librabry](https://www.gnu.org/software/gsl/manual/html_node/index.html#Top) 
is used for random numbers, special functions, ...  

The [Jansson](http://www.digip.org/jansson/) library is used
for configuring and recording some simulation results in the 
[JSON](http://www.json.org/) format. It is however, not used for the main library `sbm`
which may be used as a standalone library.

The [log](https://github.com/rxi/log.c) library is uѕed for logging operations and is included in the source code.

The executable are compiled using [Cmake](https://cmake.org). 

INSTALLATION
============

``` sh
#0 If any, install Cmake, the GSL and Jansson library
#1. Create a directory sbm
mkdir sbm
cd sbm
#2. Clone sbm-core
git clone https://gitlab.inria.fr/gpichot/sbm-core.git
#3. Create at the same level of sbm-core a directory `build`
mkdir build 
cd build
#4. Execute (up to changing the paths or .so to dylib)
cmake -DGSL_INCLUDE_DIR=/usr/include/gsl -DGSL_LIBRARY=/usr/local/lib/libgsl.so -DGSLCBLAS_LIBRARY=/usr/local/lib/libgslcblas.so -DJANSSON_INCLUDE_DIR=/usr/local/include/ -DJSANSSON_LIBRARY=/usr/local/lib/libjansson.so ../sbm-core
make
```

The executable are in a `bin` directory at the same level as the `build` directory.

The sbm library is in a `lib` directory at the same level as the `build` directory.

SRC_SBM
=======

C codes in the directory 'src_sbm'. These files aims at being compiled as a library. 

* `sbm.c`, `sbm.h` The core file, contains functions for simulating the Skew Brownian 
    motion and related processes. Compiled as a library.

SRC_BENCHMARKS
==============

C codes in the directory 'src_benchmarks'. These files aims at being compiled as executable. 

All the executable files accept as argument the name of a configuration file. 

The configuration files are defined using the JSON format and examples are proposed in the directory 'parameters'.

Each executable produces at least one file `coefficients_<method>_<suffix>`
containing the parameters and results related to the simulation. This allows
to replay the simulation if needed. 

Benchmarks: 
-----------

* `benchmark_comparison_density.c` Allows one to compare the density with the true one in free space.
    (benchmark test: Density)
* `benchmark_layer.c` Media with periodic boundary conditions and a small layer.
    (benchmark test: Layer)
* `benchmark_bimaterial_reflecting.c` Bimaterial media with two reflecting conditions
    (benchmark test: Bimaterial)
* `benchmark_bimaterial_absorbing_reflecting.c` Bimaterial media with one reflecting conditions and one absorbing condition
    (benchmark test: Bimaterial I & II)
* `benchmark_symmetry.c` Bimaterial media with one reflecting conditions and one absorbing condition
    (benchmark test: Symmetry)
* `benchmark_bimaterial_absorbing.c` Bimaterial media with two absorbing conditions
    (it does not correspond to some benchmark defined in the corresponding article, although
    some could be defined from it). 

UTILITARIES
===========

C codes in the directory 'utilitaries'
--------------------------------------

* `sbm_error_code.h` Error codes.
* `stopwatch.c`, `stopwatch.h` Utilities for recording the execution time.
* `ziggurat.c`, `ziggurat.h` Implementation of the Zigguration method 
    for simulation Gaussian random numbers, the one of the GSL 
    being not reliable. 
* `api_json.c`, `api_json.h` Utilities for JSON objects (reading, parsing), ...
* `api_config.c`, `api_config.h` Utility for reading the global configuration file.
* `sbm_error_code.h` Header for error codes for exiting programs.

C codes in the directory 'utilitaries_bimaterial'
------------------------------------------------

* `extra_benchmark.c`, `extra_benchmark.h` Utilities for Bimaterial benchmarks.


PARAMETERS
===========

Examples of configuration files.

The configuration files are defined using the JSON format. Each benchmark test
has his own configuration file. 

The configuration files are described in the files started by 'schema_'.

A 'suffix' (string) shall be specified in the simulation.
The 'suffix' is used to distinguished the outputs when several simulations
are performed with the ''same''' parameters.

A 'prefix' (string) shall be specified in the simulation.
Typically, the 'prefix' is used to specify which benchmark is run.

A 'directory' may be specified to set the output directory of the simulation. If none, the default directory is the one of the configuration JSON file.

The results of the simulations are stored in the directory 'directory/prefix/method' to distinguish the outputs when several simulations
are pertormed with ''different'' parameters.


EXTERNAL
========

External C codes

C codes in the directory 'external/log'
--------------------------------------

* `log.c`, `log.h` For logging C codes.


DOCUMENTATION
=============

The code is documented using [Doxygen](www.doxygen.org).

The file `sbm-documentation.bib` contains the bibliographical entries
in the bibtex format.
