PROJECT(SBM)
cmake_minimum_required (VERSION 2.8.3)

set(CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH} /usr/local/lib)
set(CMAKE_INCLUDE_PATH ${CMAKE_INCLUDE_PATH} /usr/local/include/)

# set the directory to store the executables
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../bin/)

# set the directory to store the shared (dynamic) libraries
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../lib/)

# set the directory to store the static libraries
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../libstatic/)


# executables are in Release version
set(CMAKE_BUILD_TYPE "Release")
message(STATUS "CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE}")

# external dependencies: GSL, JANSSON-c
find_package(GSL)

#use the -D option in the cmake command line CMAKE to set these variables
SET(JANSSON_INCLUDE_DIR ${JANSSON_INCLUDE_DIR}) #/path/to/include/jansson
SET(JANSSON_LIBRARY ${JANSSON_LIBRARY}) # /path/to/lib/libjansson.so

include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/src_benchmarks
  ${CMAKE_CURRENT_SOURCE_DIR}/src_sbm
  ${CMAKE_CURRENT_SOURCE_DIR}/utilitaries
  ${CMAKE_CURRENT_SOURCE_DIR}/utilitaries_bimaterial
  ${CMAKE_CURRENT_SOURCE_DIR}/external/log
  ${GSL_INCLUDE_DIR}
  ${JANSSON_INCLUDE_DIR}
)

FILE(GLOB UTIL_EXT_LOG external/log/*.c) # for log output

#internal dependencies
FILE(GLOB UTIL_CFILES_BIMAT utilitaries_bimaterial/*.c)
FILE(GLOB UTIL_CFILES_OTHER utilitaries/*.c)
FILE(GLOB UTIL_CFILES_SBM src_sbm/sbm.c)

# bimaterial_*  
FILE(GLOB SRC_CFILES_BIMAT src_benchmarks/benchmark_bimaterial*.c)
MESSAGE(STATUS ${SRC_CFILES_BIMAT})
# loop on the files in the list SRC_CFILES_BIMAT to create the bimaterial executables
FOREACH(CFILE ${SRC_CFILES_BIMAT})
  # get filename WE=Without Extension
  MESSAGE(STATUS ${CFILE})
  GET_FILENAME_COMPONENT(FILENAME ${CFILE} NAME_WE)
  MESSAGE(STATUS ${FILENAME})
  # generate the executable
  ADD_EXECUTABLE(${FILENAME} ${CFILE} ${UTIL_CFILES_SBM} ${UTIL_CFILES_OTHER} ${UTIL_CFILES_BIMAT} ${UTIL_EXT_LOG})
  
  TARGET_LINK_LIBRARIES(${FILENAME} ${GSL_LIBRARY} ${GSL_CBLAS_LIBRARY} ${JANSSON_LIBRARY} m)   
ENDFOREACH(CFILE)

# benchmark_comparison_density.c, benchmark_layer.c and benchmark_symmetry.c
FILE(GLOB SRC_CFILES_OTHER src_benchmarks/benchmark_comparison_density.c src_benchmarks/benchmark_layer.c src_benchmarks/benchmark_symmetry.c)
# loop on the files in the list SRC_CFILES_OTHER to create the bimaterial executables
FOREACH(CFILE ${SRC_CFILES_OTHER})
  # get filename WE=Without Extension
  GET_FILENAME_COMPONENT(FILENAME ${CFILE} NAME_WE)
  MESSAGE(STATUS ${FILENAME})

  # generate the executable
  ADD_EXECUTABLE(${FILENAME} ${CFILE} ${UTIL_CFILES_SBM} ${UTIL_CFILES_OTHER} ${UTIL_EXT_LOG})
  
  TARGET_LINK_LIBRARIES(${FILENAME} ${GSL_LIBRARY} ${GSL_CBLAS_LIBRARY} ${JANSSON_LIBRARY} m)

ENDFOREACH(CFILE)

#create library SBM
set(MYLIB sbm)
set(MYLIBSTATIC sbmstatic)
ADD_LIBRARY(${MYLIB} SHARED ${UTIL_CFILES_SBM} ${UTIL_CFILES_OTHER})
ADD_LIBRARY(${MYLIBSTATIC} STATIC ${UTIL_CFILES_SBM} ${UTIL_CFILES_OTHER})
TARGET_LINK_LIBRARIES(${MYLIB} ${GSL_LIBRARY} ${GSLCGBLAS_LIBRARY} ${JANSSON_LIBRARY})
TARGET_LINK_LIBRARIES(${MYLIBSTATIC} ${GSL_LIBRARY} ${GSLCGBLAS_LIBRARY} ${JANSSON_LIBRARY})



