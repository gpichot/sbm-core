/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */


#include "sbm.h"
#include "assert.h"
#include "gsl/gsl_math.h" 
#include "gsl/gsl_sf_log.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include "gsl/gsl_histogram2d.h"
#include <dirent.h> // for directory manipulations
#include "api_json_jansson.h"
#include "api_rng.h" 
#include "api_config.h" 
#include "stopwatch.h"
#include "sbm_error_code.h"
#include "log.h"

/** \brief Particle moving in a composite media with two discontinuities
 * and reflecting boundary conditions. 
 * Test for the symmetry of the transition density function.
 * Basically, this program computes an approximation of the density transition function. 
 *
 * @file benchmark_symmetry.c
 *
 * @author Antoine Lejay
 * @author Géraldine Pichot
 *
 * The parameters are passed through a JSON file as a set of key/value pairs. The file shall contain
 *  - L0 length of the left part
 *  - L1 length of the right part
 *  - D0 diffusivity of the left part
 *  - D1 diffusivity of the right part
 *  - deltat Time step of the algorithm
 *  - nb Number of particles
 *  - nb_steps Number of steps
 *  - method The name of the method, among "uffink", "hmyla", "sbm", "sbmlin","nocorr","hmylanormal","uffinknormal"
 *  - suffix A suffix added to the names of the files.
 *  - directory The directory for the output 
 *
 *  If no filename is given as argument, then it uses \p symmetry.json.
 *
 *  The output files (the suffix is added to the names of the files) are: 
 *  - coefficients: the input parameters [JSON].
 *  - histogram (one per time step) : 2D approximations of the density at each time step.
 *
 */

/* Associated files 
 * sbm.c, sbm.h, symmetry.json and json, GSL.
 *
 */

/** \defgroup benchmark_symmetry Benchmark test for testing symmetry properties of the schemes. 
 *
 * Benchmark test for checking if a method respect the symmetry of the density transition 
 * function associated to a divergence form operator. 
 *
 * For this, the particles evolves in a media with a single interface separating 
 * two different diffusivities and reflecting boundary conditions. The domain 
 * is discretized and simulations are performed for any point in a regular 
 * grid. A 2d-histogram is then computed to record the position on each
 * bin of the grid for each time step. This histogram gives an approximation of the density
 * transition function at several times.
 * */

/** @{ */




/** \brief Structure for the parameters of the program.
 *
 * This structure is used to store the parameters of the program. It is initialized
 * through the content of a JSON file.
 * */
typedef struct {
    /** Parameters contained in the file and that are only used in the initialization step. */
    /** @brief The JSON object from which the structure is initialized.
     * The content of the JSON object is copied into the structure. We however keep a copy of it. */
    json_obj *obj;
    /** @brief Time step. */
    double deltat;
    /** @brief Number of particles. */
    unsigned long int nb;
    /** @brief Size for the histogram for the x-position. */
    size_t histo_nx;
    /** @brief Size for the histogram for the y-position. */
    size_t histo_ny;
    /** @brief Number of steps. */
    size_t nb_steps;
    /** @brief The evolution method around the interface.*/
    double (*method)(api_rng *, media *, time_struct *, double );
    /** @brief The evolution method outside the interface layer.*/
    double (*method_outlayer)(api_rng *, double, time_struct *, double ); 
    /** @brief Name of the directory of the current simulation. */
    char prefix[50];
    /** @brief Suffix for files. */
    char suffix[50];
    /** @brief Name of the output directory. */
    char directory[512];
} coefficients;

/**@brief Initialization of the parameters from a JSON file.
 *
 * @param filename Name of the file contening the parameters in JSON format.
 * */
coefficients *coefficients_init(char *filename)
{
    coefficients *coeff=malloc(sizeof(coefficients));
    coeff->obj=json_obj_init_from_file(filename);
    coeff->deltat=json_obj_read_double(coeff->obj,"deltat");
    assert(coeff->deltat>0);
    coeff->nb=(size_t)json_obj_read_long_int(coeff->obj,"nb");
    assert(coeff->nb>0);
    // read the number of steps
    coeff->nb_steps=(size_t)json_obj_read_long_int(coeff->obj,"nb_steps");
    assert(coeff->nb_steps>0);
    // read the data for the histograms
    coeff->histo_nx=(size_t)json_obj_read_long_int(coeff->obj,"histo_nx");
    assert(coeff->histo_nx>0);
    coeff->histo_ny=(size_t)json_obj_read_long_int(coeff->obj,"histo_ny");
    assert(coeff->histo_ny>0);
    // read the method
    if(!json_obj_read_string(coeff->obj,"method"))
      { exit_on_ERROR_MISSING_METHOD; }
    if (json_obj_string_equal(coeff->obj,"method","hmyla"))
    {
	log_info("Method: hmyla\n");
	coeff->method=&diff_r_hmyla;
	coeff->method_outlayer=&outlayer_step_uniform;
    }
    else if (json_obj_string_equal(coeff->obj,"method","hmylanormal"))
    {
	log_info("Method: hmylanormal\n");
	coeff->method=&diff_r_hmyla;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","uffink"))
    {
	log_info("Method: uffink\n");
	coeff->method=&diff_r_uffink;
	coeff->method_outlayer=&outlayer_step_uniform;
    }
    else if (json_obj_string_equal(coeff->obj,"method","uffinknormal"))
    {
	log_info("Method: uffinknormal\n");
	coeff->method=&diff_r_uffink;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbm"))
    {
	log_info("Method: sbm\n");
	coeff->method=&diff_r_sbm_twosteps;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","nocorr"))
    {
	log_info("Method: nocorr\n");
	coeff->method=&diff_r_nocorr_normal;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbmlin"))
    {
	log_info("Method: sbmlin\n");
	coeff->method=&diff_r_sbmlin;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else
      { exit_on_IMPROPER_METHOD_NAME(json_obj_read_string(coeff->obj,"method")); }
    SET_SUFFIX(coeff->suffix,coeff->obj);
    SET_PREFIX(coeff->prefix,coeff->obj);
    CREATE_DIRECTORY(coeff,filename);
    return(coeff);
}

/** \brief Structure for the position (in time and space) of the particle.
 *
 * This structure is updated at each time step.
 * */
typedef struct {
    /** Position */
    double xpos;
    /** Time */
    double time;
} particle;


/** \brief Structure to store data related to one interface. */
typedef struct {
    /** \brief Structure for the media.*/
    media *media;
    /** \brief Time step and its square root.*/
    time_struct *time;
    /** \brief Position of the left-end limit of the interface layer.*/
    double left_layer;
    /** \brief Position of the right-end limit of the interface layer.*/
    double right_layer;
    /** \brief Position of the interface */
    double position;
} onestep_disc_struct;

/** \brief Check if the particle is in the layer. 
 *
 * @param[in] os Structure related to one discontinuity.
 * @param[in] pos Current position of the particle.
 * @return True (1) or false (0) whether or not the particle is in the interface layer.
 *
 * */
int onestep_disc_struct_is_in_layer(onestep_disc_struct *os,double pos)
{
    return((pos>=os->left_layer) && (pos<=os->right_layer));
}


/** \brief Change the position of a process generated by a divergence form operator with a given algorithm around an interface. 
 *
 * The time and the position are updated.
 *
 * @param[in,out] rng Random Number Generator.
 * @param[in] os Structure containing the interface.
 * @param[in,out] pos Pointer to the position, updated.
 * @param[in,out] time Point to the time, updated.
 * @param[in] ds Pointer to the method to call to update the position of the particle. @
 * @return Void.
 */
void onestep_disc_struct_next(api_rng *rng,onestep_disc_struct *os,double *pos,double *time,double (*ds)(api_rng *, media *, time_struct *, double ))
{
    (*time)+=os->time->time;
    (*pos)=(*ds)(rng,os->media,os->time,(*pos)-os->position)+os->position;
    return;
}

/** \brief Initialize a onestep structure.
 * @param[in] d1 Diffusion coefficient on the left of the interface.
 * @param[in] d2 Diffusion coefficient on the right of the interface.
 * @param[in] position Position of the interface.
 * @param[in] deltat Time step.
 * @return A new structure gathering data about interfaces.
 */
onestep_disc_struct *onestep_disc_struct_init(double d1, double d2, double position, double deltat) 
{
    onestep_disc_struct *os;
    os=malloc(sizeof(onestep_disc_struct));
    os->media=media_init(d1,d2,LAYER_FACTOR);
    os->time=time_struct_init(deltat);
    os->left_layer=position-os->time->sqrt_time*(LAYER_FACTOR)*os->media->sqrtd1;
    os->right_layer=position+os->time->sqrt_time*(LAYER_FACTOR)*os->media->sqrtd2;
    os->position=position;
    return(os);
}

/** \brief Structure for defining the media and encoding its geometry. */
typedef struct {
    /** \brief Diffusion coefficient of the left media */
    double D0;
    /** \brief Square root of the diffusion coefficient of the left media. */
    double sqrtD0;
    /** \brief Diffusion coefficient of the right media. */
    double D1;
    /** \brief Square root of the diffusion coefficient of the right media. */
    double sqrtD1;
    /** \brief Time step for the particle. */
    double deltat;
    /** \brief Structure for the time step of the particle. */
    time_struct *time;
    /** \brief Length of the left and right media.*/
    double L0;
    /** \brief Length of the left and right media.*/
    double L1;
    /** \brief Structure for dealing with the interfaces. */
    onestep_disc_struct *os1;
    /** \brief Total length of the cell */
    double length;
} struct_media;


/**\brief Tranforms the media into a JSON object. 
 *
 * This is used to save the parameters. 
 *
 * @param[in] med The media
 * @return A JSON object
 * */
json_obj *struct_media_to_json(struct_media *med)
{
    json_obj *obj=json_obj_init(); 
    json_obj_add_string(obj,"type","bimaterial");
    json_obj_add_double(obj,"D0",0.5*med->D0);
    json_obj_add_double(obj,"D1",0.5*med->D1);
    json_obj_add_double(obj,"L0",med->L0);
    json_obj_add_double(obj,"L1",med->L1);
    json_obj *layers=json_arr_init();
    json_arr_push_double(layers,0.0);
    json_arr_push_double(layers,med->os1->left_layer);
    json_arr_push_double(layers,med->os1->position);
    json_arr_push_double(layers,med->os1->right_layer);
    json_arr_push_double(layers,med->length);
    json_obj_add_obj(obj,"layers",layers);
    json_obj_add_double(obj,"length",med->length);
    json_obj_add_double(obj,"interface",med->L0);
    json_obj_add_string(obj,"left_BC","reflection");
    json_obj_add_string(obj,"right_BC","reflection");
    return(obj);
}


/** @brief Initialization of a structure with the media.
 *
 * @warning As this method simulates the process generated by the operator \f$\nabla(D\nabla\cdot) \f$
 * so that the diffusion coefficients D0 and D1 are multiplied by 2.
 *
 * The media characteristics are stored as JSON key/values pair in \p coeff->obj.
 *
 * @param[in,out] coeff The parameters stored in a \p coefficients structure.
 * @return A new \p struct_media.
 *
 */
struct_media *struct_media_init(coefficients *coeff)
{
    struct_media *med;
    med=malloc(sizeof(struct_media));
    // Simulation with the one-half factor
    med->D0=2*json_obj_read_double(coeff->obj,"D0");
    assert(med->D0>0.0); // if the key is not defined, then med->D0 is set to 0.0
    med->sqrtD0=sqrt(med->D0);
    med->D1=2*json_obj_read_double(coeff->obj,"D1");
    assert(med->D1>0.0); // if the key is not defined, then med->D1 is set to 0.0
    med->sqrtD1=sqrt(med->D1);
    med->deltat=coeff->deltat;
    med->time=time_struct_init(coeff->deltat);
    med->L0=json_obj_read_double(coeff->obj,"L0");
    assert(med->L0>0.0);
    med->L1=json_obj_read_double(coeff->obj,"L1");
    assert(med->L1>0.0);
    med->length=med->L0+med->L1;
    med->os1=onestep_disc_struct_init(med->D0,med->D1,med->L0,coeff->deltat);
    if(med->os1->left_layer<0.0)
    {
	log_error("The left layer is at %f and is negative\n",med->os1->left_layer);
	exit(ERROR_IMPROPER_CONFIGURATION);
    }
    if(med->os1->right_layer>med->length)
    {
	log_error("The left layer is at %f and is bigger than the length (%f)\n",med->os1->right_layer,med->length);
	exit(ERROR_IMPROPER_CONFIGURATION);
    }
    json_obj *obj_media=struct_media_to_json(med);
    json_obj_add_obj(coeff->obj,"media",obj_media);
    return(med);
}

/** \brief Print the histogram as a matrix.
 *
 * @param file output stream.
 * @param histo 2d-histogram to print.
 * @return Void.
 */
void histogram_print_matrix(FILE *file,gsl_histogram2d *histo)
{
    size_t i,j;
    for(i=0;i<histo->nx;i++)
    {
	for(j=0;j<histo->ny-1;j++)
	{
	    fprintf(file,"%g\t",histo->bin[i*histo->ny+j]);
	}
	fprintf(file,"%g\n",histo->bin[i*histo->ny+histo->ny-1]);
    }
}

/** Binary search for the y-value of the histogram
 *
 * @param histo 2d-histograms
 * @param value value to search
 * @return the indice \p i of the bin, that is such that histo->yrange[i]<=value<histo->yrange[i+1]
 */
size_t binary_search_yrange(gsl_histogram2d *histo, double value)
{
    size_t down,up,middle;
    unsigned int greater;
    down=0;
    up=histo->ny;
    while(up>down+1)
    {
	middle=(down+up)/2;
	greater=(value>=histo->yrange[middle]);
	if(greater && (value<histo->yrange[middle+1]))
	    {return(middle);}
	if(greater)
	    { down=middle; }
	else
	{ up=middle;}
    }
   assert((value>=histo->yrange[down] && value<histo->yrange[down+1]));
   return(down);
}

/** @brief Simulation of one step for the particle moving in the media.
 * @param[in,out] rng Random Number Generator.
 * @param[in] med Structure with the interface definition.
 * @param[in,out] part Position of the particle. 
 * @param coeff Coefficients.
 * @return void
 */
void onestep(api_rng *rng, struct_media *med, particle *part, coefficients *coeff)
{
    // Simulation close to the interfaces of the layer.
    if(onestep_disc_struct_is_in_layer(med->os1,part->xpos))
    {
	onestep_disc_struct_next(rng,med->os1,(&(part->xpos)),(&(part->time)),(coeff->method));
	if(part->xpos<=0.0)
	    { part->xpos=-part->xpos; } // reflection 
	else if (part->xpos>=med->length)
	    { part->xpos=2*med->length-part->xpos;} // reflection 
	return;
    }
    // outlayer step in the left with reflection
    if(part->xpos<=med->os1->position)
	{
	part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD0,med->time,part->xpos);
	part->time+=med->time->time;
	part->xpos=fabs(part->xpos);
	return;
	}
    // Outlayer step in the right with reflection
    part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD1,med->time,part->xpos);
    part->time+=med->time->time;
    if(part->xpos<=0.0)
	{
	    part->xpos=-part->xpos; 
	} // reflection on the left
    if(part->xpos>=med->length)
	{
	    part->xpos=2*med->length-part->xpos; 
	} // reflection on the right
    return;
}

/** If no argument is passed, use the file "symmetry.json" for specification. 
 * Otherwise, use the first argument as the name of a JSON file. */
int main(int argc, char *argv[])
{

    log_info("Benchmark %s\n",basename(__FILE__));

    // For the computation time
    stopwatch *sw=stopwatch_init();

    // Initialize the random number generator.
    api_rng * rng=api_rng_init();

    size_t i,j,steps;
    FILE *file;

    // For the filenames
    json_obj *filesnames =json_obj_init();

    // For the coefficients stored in a JSON file.
    coefficients *coeff;

    READ_CONFIGURATION_FILE(coeff);

    // initialialization of the structure
    particle *part;
    part=malloc(sizeof(particle));
    struct_media *med;
    med=struct_media_init(coeff);

    // initialization of the histograms
    gsl_histogram2d *histo[coeff->nb_steps];
    for(steps=0;steps<coeff->nb_steps;steps++)
    {
	histo[steps]=gsl_histogram2d_alloc (coeff->histo_nx,coeff->histo_ny);
	gsl_histogram2d_set_ranges_uniform(histo[steps],0.0,med->length,0.0,med->length);
    }

    fprintf(stderr,"%3.0f%%",0.0);

    // size of a bin, it is the same for all the histograms
    double size_bin=histo[0]->xrange[1]-histo[0]->xrange[0]; 
    double init_pos;
    size_t search;

    // ratio for the counter
    double ratio=100/((double) coeff->nb);

    stopwatch_start(sw);

    // THE MAIN LOOP
    for(i=0;i<coeff->nb;i++)
	{
	fprintf(stderr,"\r%3.0f%%",ratio*i);
	for(j=0;j<coeff->histo_nx;j++)
	    {
	    part->xpos=init_pos=(j+0.5)*size_bin;
	    part->time=0.0;
	    for(steps=0;steps<coeff->nb_steps;steps++)
		{
		onestep(rng,med,part,coeff);  // position in part->xpos
		search=binary_search_yrange(histo[steps],part->xpos);
		histo[steps]->bin[j*histo[steps]->ny+search]++; // increment the histogram
		}
	    }
	}
    fprintf(stderr,"\r\n");
    // END OF THE MAIN LOOP

    stopwatch_stop(sw); // stop the stopwatch 
    stopwatch_show(sw,stdout); // show the execution time
    json_obj_add_timestamp(coeff->obj); // records the current time
    json_obj_add_double(coeff->obj,"usertime",stopwatch_usertime(sw)); // records the exectution time
    free(sw); // free the stopwatch
    
    /* Write the ranges of the 2d-histogram into the JSON object as an array */
    json_obj *xarray =json_arr_init();
    for(i=0;i<=histo[0]->nx;i++)
	{ json_arr_push_double(xarray,histo[0]->xrange[i]); }
    json_obj *yarray =json_arr_init();
    for(i=0;i<=histo[0]->ny;i++)
	{ json_arr_push_double(yarray,histo[0]->yrange[i]); }


    // For the names of the files
    char histogram_filename[512];
    char name[512];
    char coefficients_filename[1024];

    json_obj *histo_filenames =json_arr_init();

    for(steps=0;steps<coeff->nb_steps;steps++)
	{
	// Renormalization as a density
	gsl_histogram2d_scale(histo[steps],coeff->histo_ny/(coeff->nb*med->length));
	// Create the name of the output file
	sprintf(name,"histo_%s%s_%li",json_obj_read_string(coeff->obj,"method"),coeff->suffix,steps+1);
	sprintf(histogram_filename,"%s/%s",coeff->directory,name); // concatenate the directory and the name
	json_arr_push_string(histo_filenames,histogram_filename);
	FILE_OPEN_OR_DIE(file,histogram_filename,"w");
	histogram_print_matrix(file,histo[steps]);
	fclose(file);
	gsl_histogram2d_free(histo[steps]);
	}

    /* Write informations about the benchmark test in a JSON object. */
    json_obj_add_long_int(coeff->obj,"layer_factor",LAYER_FACTOR);
    json_obj_add_string(coeff->obj,"directory",coeff->directory);
    json_obj_add_string(coeff->obj,"benchmark",basename(__FILE__));
    json_obj_add_long_int(coeff->obj,"histo_nx",coeff->histo_nx);
    json_obj_add_long_int(coeff->obj,"histo_ny",coeff->histo_ny);
    json_obj_add_obj(coeff->obj,"xrange",xarray);
    json_obj_add_obj(coeff->obj,"yrange",yarray);
    // add the git commit, comment this line if GIT is not used
    json_obj_add_commit(coeff->obj,__FILE__);

    /** Save the coefficients into a file with the JSON format. */
    sprintf(coefficients_filename,"%s/coefficients_%s%s.json",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    log_info("Coefficients written in %s\n",coefficients_filename);

    json_obj_add_string(filesnames,"coefficients",coefficients_filename);
    json_obj_add_obj(filesnames,"histogram",histo_filenames);

    json_obj_add_obj(coeff->obj,"files",filesnames); // Add the filenames to the JSON object

    json_obj_to_file(coeff->obj,coefficients_filename);
    json_obj_put(coeff->obj); // Free the coefficients
    free(coeff);
    api_rng_free(rng); // free the rng
    /** Leave without error.*/
    return(EXIT_SUCCESS);
}

/** @} */


