/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

#include "sbm.h"
#include "assert.h"
#include "gsl/gsl_sf_log.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <sys/times.h>
#include <stdint.h>  // types of integer (including size_t)
#include <unistd.h> // Standard types 
#include "api_json_jansson.h" // JSON files 
#include "api_config.h" // JSON files 
#include <dirent.h> // for directory manipulations
#include "stopwatch.h" // Stopwatch
#include "extra_benchmark.h"
#include "api_rng.h"
#include "log.h"



/** @brief Particle moving in a composite media with two discontinuities
 * and reflecting boundary conditions. Compute only the proportions
 * on each side of the interface.
 *
 * @file benchmark_bimaterial_reflecting.c
 *
 * @author Antoine Lejay 
 * @author Géraldine Pichot
 *
 * @date 2019-01-03
 * @version 1.0
 *
 * The parameters are passed through a JSON file as a set of key/value pairs. The file shall contain
 *  - L0 length of the left part
 *  - L1 length of the right part
 *  - D0 diffusivity of the left part
 *  - D1 diffusivity of the right part
 *  - final_time Final Time
 *  - deltat Time step of the algorithm
 *  - nb Number of particles
 *  - starting A string specifying if the starting point is uniform or fixed (values: "uniform" or "fixed")
 *  - starting_point Posistion of the starting point, if fixed
 *  - method The name of the method, among "uffink", "hmyla", "sbm", "sbmlin"
 *  - suffix A suffix added to the names of the files.
 *  - directory The directory for writing the results 
 *
 *  The output files (the suffix is added to the names of the files) are: 
 *  - coefficients: the input parameters [JSON]
 *  - proportions: the proportion of particles on each side of the interface,
 *  and number of crossings, at sample times. 
 *
 */

/** \addtogroup benchmark_bimaterial_reflecting */
/** @{ */


/**@brief Initialization of the parameters from a JSON file.
 *
 * @param[in] filename Name of the file contening the parameters in JSON format.
 * @return The coefficients
 *
 * @warning The validity of the parameters are checked, otherwise errors (or a segmentation fault error) are raised
 * and the program stops.
 */
coefficients *coefficients_init(char *filename)
{
    coefficients *coeff=malloc(sizeof(coefficients));
    coeff->obj=json_obj_init_from_file(filename);
    coeff->final_time=json_obj_read_double(coeff->obj,"final_time");
    assert(coeff->final_time>0);
    coeff->deltat=json_obj_read_double(coeff->obj,"deltat");
    assert(coeff->deltat>0);
    coeff->nb=(size_t)json_obj_read_long_int(coeff->obj,"nb");
    assert(coeff->nb>0);

    // read informations about the starting point.
    if(strcmp(json_obj_read_string(coeff->obj,"starting"),"fixed")==0)
    {
	coeff->starting_point=json_obj_read_double(coeff->obj,"starting_point");
	assert(coeff->starting_point<json_obj_read_double(coeff->obj,"L0")+json_obj_read_double(coeff->obj,"L1"));
	assert(coeff->starting_point>0.0);
	coeff->starting=FIXED_SP;
	log_info("Fixed starting point at %f\n",coeff->starting_point);
    }
    else if (strcmp(json_obj_read_string(coeff->obj,"starting"),"uniform")==0)
    {
	coeff->starting=UNIFORM_SP;
	log_info("Uniform starting point");
    }
    else
    {
	log_error("Starting point not well defined.");
	exit(ERROR_IMPROPER_STARTING_POINT_CONFIGURATION);
    }
    if(!json_obj_read_string(coeff->obj,"method"))
    {
	exit_on_ERROR_MISSING_METHOD;
    }
    if (json_obj_string_equal(coeff->obj,"method","hmyla"))
    {
	log_info("Method: hmyla\n");
	coeff->method=&diff_r_hmyla;
	coeff->method_outlayer=&outlayer_step_uniform;
    }
    else if (json_obj_string_equal(coeff->obj,"method","hmylanormal"))
    {
	log_info("Method: hmylanormal\n");
	coeff->method=&diff_r_hmyla;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","uffink"))
    {
	log_info("Method: uffink\n");
	coeff->method=&diff_r_uffink;
	coeff->method_outlayer=&outlayer_step_uniform;
    }
    else if (json_obj_string_equal(coeff->obj,"method","uffinknormal"))
    {
	log_info("Method: uffinknormal\n");
	coeff->method=&diff_r_uffink;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbm"))
    {
	log_info("Method: sbm\n");
	coeff->method=&diff_r_sbm_twosteps;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbmlin"))
    {
	log_info("Method: sbmlin\n");
	coeff->method=&diff_r_sbmlin;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else
	{ exit_on_IMPROPER_METHOD_NAME(json_obj_read_string(coeff->obj,"method"));}

    SET_SUFFIX(coeff->suffix,coeff->obj);
    SET_PREFIX(coeff->prefix,coeff->obj);
    CREATE_DIRECTORY(coeff,filename);
    return(coeff);
}



/** @brief Simulation of the particle in the media.
 *
 * @param[in,out] rng Random number generator.
 * @param[in] med The media.
 * @param[in,out] part The particle.
 * @param coeff The coefficient
 */
void onestep(api_rng *rng, struct_media *med, particle *part, coefficients *coeff)
{
    double x;
    // Simulation close to the interfaces of the layer.
    if(onestep_disc_struct_is_in_layer(med->os1,part->xpos))
    {
	onestep_disc_struct_next(rng,med->os1,(&(part->xpos)),(&(part->time)),(coeff->method));
	if(part->xpos<=0.0)
	    { part->xpos=-part->xpos; } // reflection at the left endpoint
	else if (part->xpos>=med->length)
	    { part->xpos=2*med->length-part->xpos;} // reflection at the right endpoint
	return;
    }
    // outlayer step in the left with reflection
    if(part->xpos<=med->os1->position)
	{
	part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD0,med->time,part->xpos);
	part->time+=med->time->time;
	part->xpos=fabs(part->xpos); // reflection at the left endpoint
	return;
	}
    // Outlayer step in the right with reflection
    part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD1,med->time,part->xpos);
    part->time+=med->time->time;
    if(part->xpos>med->length)
	{ part->xpos=2*med->length-part->xpos; } // reflection on the right
    return;
}

/** @brief the main part.
 *
 * @param argc Number of arguments
 * @param argv Vector of arguments. 
 *
 * Use the first argument as the name of a JSON file. */
int main(int argc, char *argv[])
{
    log_info("Benchmark %s\n",basename(__FILE__));

    // For the computation time
    stopwatch *sw=stopwatch_init();

    // For the coefficients
    coefficients *coeff;

    // For the filenames
    json_obj *filesnames =json_obj_init();

    READ_CONFIGURATION_FILE(coeff);

    // rng initialization
    api_rng * rng=api_rng_init();

    unsigned int i,j;
    FILE *file,*file_prop,*file_coeff;


    double ratio = 100.0/((double) coeff->nb);


    double next_time,prev_time;

    // For counting the number of crossings
    size_t nleft,nright;
    size_t ncrossingpm,ncrossingmp;

    double positions[coeff->nb];

    char proportions_name[256], proportions_filename[512], coefficients_filename[512];

    particle *part;
    part=malloc(sizeof(particle));
    struct_media *med;
    med=struct_media_init(coeff);

    // File for the proportions 
    sprintf(proportions_name,"proportions_%s%s",json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    sprintf(proportions_filename,"%s/%s",coeff->directory,proportions_name);
    FILE_OPEN_OR_DIE(file_prop,proportions_filename,"w");
    fclose(file_prop);

    // Initializatio of the starting points 
    fprintf(stderr,"%3.0f%%",0.0);
    if(coeff->starting==FIXED_SP)
	{
	for(i=0;i<coeff->nb;i++)
	    { positions[i]=coeff->starting_point;}
	}
    if(coeff->starting==UNIFORM_SP)
	{
	for(i=0;i<coeff->nb;i++)
	    { positions[i]=api_rng_uniform(rng)*med->length;}
	}

    // open the file for the proportions
    FILE_OPEN_OR_DIE(file_prop,proportions_filename,"a");

    stopwatch_start(sw);
    // MAIN LOOP
    next_time=0.0;
    prev_time=0.0;
    do 
	{
	    prev_time=next_time;
	    next_time+=coeff->deltat;
	    next_time=(next_time>=coeff->final_time ? coeff->final_time : next_time);
	    nleft=nright=0;
	    ncrossingmp=ncrossingpm=0;
	    fprintf(stderr,"\r%.3g        ",next_time);
	    for(i=0;i<coeff->nb;i++)
		{
		part->xpos=positions[i];
		part->time=prev_time;
		onestep(rng,med,part,coeff); 
		if(part->xpos>=med->os1->position)
		{
		    nright++;
		    if(positions[i]<=med->os1->position)
			{ ncrossingmp++;} // count the number of crossings from left to right (minus->plus)
		}
		else
		{
		    nleft++;
		    if(positions[i]>=med->os1->position)
			{ ncrossingpm++;} // count the number of crossings from right to left (plus->minus)
		}
		positions[i]=part->xpos;
		}
	    // Print the proportions in the file
	    fprintf(file_prop,"%g\t%g\t%g\t%g\t%g\n",next_time,nleft/((double) coeff->nb),nright/((double) coeff->nb),ncrossingmp/((double) coeff->nb),ncrossingpm/((double) coeff->nb));
	} while (next_time<coeff->final_time);
    fclose(file_prop);
    fprintf(stderr,"\b\b\b\b\n");
    // END OF THE MAIN LOOP

    log_info("Proportions written in %s\n",proportions_filename);

    // Stop the stopwatch
    stopwatch_stop(sw); // stop the stopwatch 
    stopwatch_show(sw,stdout); // show the execution time
    json_obj_add_timestamp(coeff->obj); // records the current time
    json_obj_add_double(coeff->obj,"usertime",stopwatch_usertime(sw)); // records the exectution time
    free(sw); // free the stopwatch

    // save reated parameters
    json_obj *obj_media=struct_media_to_json(med);
    json_obj_add_string(obj_media,"leftBC","reflecting");
    json_obj_add_string(obj_media,"rightBC","absorbing");
    json_obj_add_long_int(obj_media,"layer_factor",LAYER_FACTOR);
    json_obj_add_obj(coeff->obj,"media",obj_media);
    // Adding informations to the coefficients file
    json_obj_add_string(coeff->obj,"directory",coeff->directory); // the directory
    // save the benchmark
    json_obj_add_string(coeff->obj,"benchmark",basename(__FILE__));
    // add the git commit, comment this line if GIT is not used
    json_obj_add_commit(coeff->obj,__FILE__);

    // Save the coefficients into a file. 
    sprintf(coefficients_filename,"%s/coefficients_%s%s.json",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    log_info("Coefficients written in %s\n",coefficients_filename);

    json_obj_add_string(filesnames,"coefficients",coefficients_filename);
    json_obj_add_string(filesnames,"proportions",proportions_filename);

    json_obj_add_obj(coeff->obj,"files",filesnames); // Add the filenames to the JSON object

    json_obj_to_file(coeff->obj,coefficients_filename); // Save the coefficients

    // Free the coefficients
    json_obj_put(coeff->obj); 
    free(coeff);
    return(0);
}


/** @} */

