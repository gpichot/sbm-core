/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

#include "sbm.h"
#include "assert.h"
#include "gsl/gsl_sf_log.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <sys/times.h>
#include <stdint.h>
#include <unistd.h>
#include "stopwatch.h"
#include <dirent.h> // for directory manipulations
#include "extra_benchmark.h"
#include "sbm_error_code.h"
#include "api_json_jansson.h"
#include "api_config.h"
#include "log.h"

/** \brief Particle moving in a composite media with two discontinuities
 * and absorbing boundary conditions.
 *
 * @file benchmark_bimaterial_absorbing.c
 *
 * @author Antoine Lejay
 * @author Géraldine Pichot
 *
 * @date 2019-01-03
 * @version 1.0
 *
 * The parameters are passed through a JSON file as a set of key/value pairs. The file shall contain
 *  - L0 length of the left part
 *  - L1 length of the right part
 *  - D0 diffusivity of the left part
 *  - D1 diffusivity of the right part
 *  - final_time Final Time
 *  - deltat Time step of the algorithm
 *  - sample_time Time step at which the proportions are computed
 *  - nb Number of particles
 *  - starting A string specifying if the starting point is uniform or fixed (values: "uniform" or "fixed")
 *  - starting_point Posistion of the starting point, if fixed
 *  - method The name of the method, among "uffink", "hymla", "sbm", "sbmlin"
 *  - suffix A suffix added to the names of the files (optional).
 *
 *  @warning If sample_time is not bigger than delat, then errors
 *  are induced.
 *
 *  The output files (the suffix is added to the names of the files) are: 
 *  - coefficients: the input parameters [JSON].
 *  - positions: positions of the particles on each side of the interface.
 *  - proportions: the proportion of particles on each side of the interface,
 *  and the total number of living particles. 
 *  - exit time: samples of the exit time.
 *
 * The width of the interface layer, inside which a SBM step is performed, 
 * is defined by LAYER_FACTOR in api_config.h.
 */


/** \addtogroup benchmark_bimaterial_absorbing */
/** @{ */

/** \brief Square root of 3
 */
extern const double SQRT3; //=1.73205080756888;


/**\brief Initialization of the parameters from a JSON file.
 *
 * @param filename Name of the file contening the parameters in JSON format.
 * @return A coefficients.
 * */
coefficients *coefficients_init(char *filename)
{
    coefficients *coeff=malloc(sizeof(coefficients));
    coeff->obj=json_obj_init_from_file(filename);
    coeff->final_time=json_obj_read_double(coeff->obj,"final_time");
    assert(coeff->final_time>0);
    coeff->deltat=json_obj_read_double(coeff->obj,"deltat");
    assert(coeff->deltat>0);
    coeff->sample_time=json_obj_read_double(coeff->obj,"sample_time");
    assert(coeff->sample_time>0);
    coeff->nb=(size_t)json_obj_read_long_int(coeff->obj,"nb");
    assert(coeff->nb>0);
    if (json_obj_string_equal(coeff->obj,"starting","fixed"))
    {
	coeff->starting_point=json_obj_read_double(coeff->obj,"starting_point");
	assert(coeff->starting_point<json_obj_read_double(coeff->obj,"L0")+json_obj_read_double(coeff->obj,"L1"));
	assert(coeff->starting_point>0.0);
	coeff->starting=FIXED_SP;
	log_info("Using a fixed starting point at %f",coeff->starting_point);
    }
    else if (json_obj_string_equal(coeff->obj,"starting","uniform"))
    {
	coeff->starting=UNIFORM_SP;
	log_info("Using uniformly distributed starting points");
    }
    else
    {
      exit_on_IMPROPER_STARTING_POINT_CONFIGURATION;
    }
    if(!json_obj_is_string(coeff->obj,"method"))
    {
      exit_on_ERROR_MISSING_METHOD;
    }
    if (json_obj_string_equal(coeff->obj,"method","hmyla"))
    {
	log_info("Method: hmyla");
	coeff->method=&diff_r_hmyla;
	coeff->method_outlayer=&outlayer_step_uniform;
	coeff->method_left_boundary=&onestep_left_absorbing_boundary_layer_uniform_linear;
	coeff->method_right_boundary=&onestep_right_absorbing_boundary_layer_uniform_linear;
    }
    else if (json_obj_string_equal(coeff->obj,"method","uffink"))
    {
	log_info("Method: uffink");
	coeff->method=&diff_r_uffink;
	coeff->method_outlayer=&outlayer_step_uniform;
	coeff->method_left_boundary=&onestep_left_absorbing_boundary_layer_uniform_linear;
	coeff->method_right_boundary=&onestep_right_absorbing_boundary_layer_uniform_linear;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbm"))
    {
	log_info("Method: sbm");
	coeff->method=&diff_r_sbm_twosteps;
	coeff->method_outlayer=&outlayer_step_normal;
	coeff->method_left_boundary=&onestep_left_absorbing_boundary_layer_normal_exact;
	coeff->method_right_boundary=&onestep_right_absorbing_boundary_layer_normal_exact;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbmlin"))
    {
	log_info("Method: sbmlin");
	coeff->method=&diff_r_sbmlin;
	coeff->method_outlayer=&outlayer_step_normal;
	coeff->method_left_boundary=&onestep_left_absorbing_boundary_layer_normal_linear;
	coeff->method_right_boundary=&onestep_right_absorbing_boundary_layer_normal_linear;
    }
    else
    {
	exit_on_IMPROPER_METHOD_NAME(json_obj_read_string(coeff->obj,"method"));
    }

    SET_SUFFIX(coeff->suffix,coeff->obj);
    SET_PREFIX(coeff->prefix,coeff->obj);
    CREATE_DIRECTORY(coeff,filename);
    return(coeff);
}



/** @brief Simulation of one step for the particle moving in the media.
 * @param[in,out] rng Random Number Generator.
 * @param[in] med The media.
 * @param[in,out] part The particle.
 * @param[in] coeff The coefficients
 * @return void.
 */
void onestep(api_rng *rng, struct_media *med, particle *part, coefficients *coeff)
{
    // Simulation close to the interfaces of the layer.
    if(onestep_disc_struct_is_in_layer(med->os1,part->xpos))
    {
	onestep_disc_struct_next(rng,med->os1,(&(part->xpos)),(&(part->time)),(coeff->method));
	if(part->xpos<=0.0)
	    { part->xpos=0.0;part->alive=0; } // absorption 
	else if (part->xpos>=med->length)
	    { part->xpos=med->length;part->alive=0;} // absorption 
	return;
    }
    // outlayer step in the left with absorption
    if(part->xpos<=med->os1->position)
	{
	    if(part->xpos<=med->limit_layer_left)
		// absorption
	    {
		coeff->method_left_boundary(rng,med,part);
		return;
	    }
	part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD0,med->time,part->xpos);
	part->time+=med->time->time;
	if(part->xpos<0.0)
	    {
		part->xpos=0.0;
		part->alive=0;
	    }
	return;
	}
    // Outlayer step in the right with absorption
    if(part->xpos>=med->limit_layer_right)
    {
	coeff->method_right_boundary(rng,med,part);
	return;
    }
    part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD1,med->time,part->xpos);
    part->time+=med->time->time;
    if(part->xpos>med->length)
	{ 
	    part->xpos=med->length;
	    part->alive=0;
	}
    return;
}


/** @brief the main part.
 *
 * @param argc Number of arguments
 * @param argv Vector of arguments. 
 *
 * If no argument is passed to the executable, raise an error
 * Otherwise, use the first argument as the name of a JSON file. */
int main(int argc, char *argv[])
{

    json_obj *filesnames = json_obj_init(); 

    log_info("Benchmark %s\n",basename(__FILE__));

    // For the computation time
    stopwatch *sw=stopwatch_init();

    // For the coefficients
    coefficients *coeff;

    READ_CONFIGURATION_FILE(coeff);

    api_rng * rng=api_rng_init();

    json_obj_add_string(coeff->obj,"rng_type",api_rng_name(rng));

    unsigned int i;
    FILE *file,*file_prop,*exit_time_file;


    // for printing the progress during the simulations
    double ratio = 100.0/((double) coeff->nb);

    double next_time,prev_time;
    unsigned int nleft,nright;
	
    double positions[coeff->nb];
    double final_positions[coeff->nb];


    char exit_times_filename[256], coefficients_filename[256], proportions_filename[256], positions_filename[256];

    particle *part;
    part=malloc(sizeof(particle));
    struct_media *med;
    med=struct_media_init(coeff);

    log_info("Writing parameters in file %s%s",json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    sprintf(proportions_filename,"%s/proportions_%s%s",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    FILE_OPEN_OR_DIE(file_prop,proportions_filename,"w");
    fclose(file_prop);

    // Open the file for the exit times
    log_info("Open the file for the exit times.");
    sprintf(exit_times_filename,"%s/exit_time_%s%s",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    FILE_OPEN_OR_DIE(exit_time_file,exit_times_filename,"w");
    
    if(coeff->starting==FIXED_SP)
	{
	for(i=0;i<coeff->nb;i++)
	    { positions[i]=coeff->starting_point;}
	}
    if(coeff->starting==UNIFORM_SP)
	{
	for(i=0;i<coeff->nb;i++)
	    { positions[i]=api_rng_uniform(rng)*med->length;}
	}

    long int nb_alive=coeff->nb; // counter for the alive particles at the beginning of the time-step.
    long int cnt_alive=0; // counter for the alive particles at the end of the time-step.

    // THE MAIN LOOP
    next_time=0.0;
    prev_time=0.0;
    // Simulation with snapshots at given times
    log_info("Simulation with snapshots at given times.\n");

    fprintf(stderr,"%3.0f%%",0.0);
    stopwatch_start(sw); // start the stopwatch
    do 
	{
	    prev_time=next_time;
	    next_time+=coeff->deltat;
	    fprintf(stderr,"\r%3.3g             ",next_time); // print the time
	    nleft=nright=0;
	    cnt_alive=0;
	    // iterate only on the living particles
	    for(i=0;i<nb_alive;i++)
	    {
		fprintf(stderr,"\b\b\b\b%3.0f%%",i*ratio);
		part->xpos=positions[i];
		part->time=prev_time;
		part->alive=1;
		onestep(rng,med,part,coeff); 
		if(part->alive==0) // check if the particle is killed
		    { fprintf(exit_time_file,"%f\n",part->time); }
		else // the particle is not killed
		    {
		    final_positions[cnt_alive++]=part->xpos; // copy the position
		    if(part->xpos>=med->os1->position)
			{ nright++; }
		    else
			{ nleft++; }
		    }
	    }
	    nb_alive=cnt_alive;
	    // Write the new positions to the old one
	    for(i=0;i<nb_alive;i++)
		{ positions[i]=final_positions[i]; }
	    /** Write the proportion of particles in each compartment.*/
	    FILE_OPEN_OR_DIE(file_prop,proportions_filename,"a");
	    fprintf(file_prop,"%f\t%g\t%g\t\n",next_time,nleft/((double) coeff->nb),nright/((double) coeff->nb));
	    fclose(file_prop);
	} while (next_time<coeff->final_time);
    fprintf(stderr,"\r\n");
    // END OF THE MAIN LOOP
    //

    fclose(exit_time_file);
    log_info("End particles simulations.\n");

    log_info("Exit times written in %s\n",exit_times_filename);
	
    stopwatch_stop(sw); // stop the stopwatch 
    stopwatch_show(sw,stdout); // show the execution time
    json_obj_add_timestamp(coeff->obj); // records the current time
    json_obj_add_double(coeff->obj,"usertime",stopwatch_usertime(sw)); // records the exectution time
    free(sw); // free the stopwatch

    log_info("Proportions written in %s\n",proportions_filename);
    /** Write the positions of particles at the final time */
    sprintf(positions_filename,"%s/positions_%s%s",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    FILE_OPEN_OR_DIE(file,positions_filename,"w");
    for(i=0;i<coeff->nb;i++)
    {
	fprintf(file,"%f\n",positions[i]);
    }
    log_info("Positions written in %s\n",positions_filename);
    fclose(file);

    // boundary conditions
    json_obj *obj_media=struct_media_to_json(med);
    json_obj_add_string(obj_media,"leftBC","absorbing");
    json_obj_add_string(obj_media,"rightBC","absorbing");
    json_obj_add_long_int(obj_media,"layer_factor",LAYER_FACTOR);
    json_obj_add_obj(coeff->obj,"media",obj_media);
    json_obj_add_string(coeff->obj,"directory",coeff->directory); // the directory
    json_obj_add_string(coeff->obj,"benchmark",basename(__FILE__));
    // add the git commit, comment this line if GIT is not used
    json_obj_add_commit(coeff->obj,__FILE__);

    /** Save the coefficients into a file. */
    sprintf(coefficients_filename,"%s/coefficients_%s%s.json",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    log_info("Coefficients written in %s\n",coefficients_filename);

    // Add the name of the file for the coefficients to the list of filenames
    json_obj_add_string(filesnames,"coefficients",coefficients_filename);
    json_obj_add_string(filesnames,"positions",positions_filename); 
    json_obj_add_string(filesnames,"proportions",proportions_filename); 
    json_obj_add_string(filesnames,"exit times",exit_times_filename); 
    json_obj_add_obj(coeff->obj,"files",filesnames); // Add the filenames to the JSON object

    json_obj_to_file(coeff->obj,coefficients_filename); // save the coefficients to a file

    json_obj_put(coeff->obj); // Free the coefficients
    free(coeff);
    api_rng_free(rng);
    return(EXIT_SUCCESS);
}

/** @} */


