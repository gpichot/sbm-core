/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

#include "sbm.h"
#include "assert.h"
#include "gsl/gsl_sf_log.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <sys/times.h>
#include <stdint.h>
#include <unistd.h>
#include "api_json_jansson.h" 
#include "api_rng.h" 
#include "api_config.h" 
#include "stopwatch.h"
#include <dirent.h> // for directory manipulations
#include "extra_benchmark.h"
#include "log.h"


/** \brief Particle moving in a composite media with two discontinuities
 * and absorbing boundary condition on the right side and reflecting on the left side.
 *
 * @file benchmark_bimaterial_absorbing_reflecting.c
 *
 * @author Antoine Lejay
 * @author Géraldine Pichot
 *
 * @date 2019-01-03
 * @version 1.0
 *
 * The parameters are passed through a JSON file as a set of key/value pairs. The file shall contain
 *  - L0 length of the left part
 *  - L1 length of the right part
 *  - D0 diffusivity of the left part
 *  - D1 diffusivity of the right part
 *  - final_time Final Time
 *  - deltat Time step of the algorithm
 *  - nb Number of particles
 *  - starting A string specifying if the starting point is uniform or fixed (values: "uniform" or "fixed")
 *  - starting_point Posistion of the starting point, if fixed
 *  - method The name of the method, among "uffink", "hmyla", "sbm", "sbmlin"
 *  - suffix A suffix added to the names of the files.
 *
 *  The output files (the suffix is added to the names of the files) are: 
 *  - coefficients: the input parameters [JSON]
 *  - proportions: the proportion of particles on each side of the interface,
 *  and the total number of living particles. 
 *  - exit time: samples of the exit time.
 */



/** \addtogroup benchmark_bimaterial_absorbing_reflecting */
/** @{ */



/**\brief Initialization of the parameters from a JSON file.
 *
 * @param filename Name of the file contening the parameters in JSON format.
 *
 * @return A coefficient.
 *
 * @warning If the parameters are miss-specified, then the program stops.
 * */
coefficients *coefficients_init(char *filename)
{
    coefficients *coeff=malloc(sizeof(coefficients));
    coeff->obj=json_obj_init_from_file(filename);
    coeff->final_time=json_obj_read_double(coeff->obj,"final_time");
    assert(coeff->final_time>0);
    coeff->deltat=json_obj_read_double(coeff->obj,"deltat");
    assert(coeff->deltat>0);
    coeff->nb=(size_t)json_obj_read_long_int(coeff->obj,"nb");
    assert(coeff->nb>0);
    if(strcmp(json_obj_read_string(coeff->obj,"starting"),"fixed")==0)
    {
	coeff->starting_point=json_obj_read_double(coeff->obj,"starting_point");
	assert(coeff->starting_point<json_obj_read_double(coeff->obj,"L0")+json_obj_read_double(coeff->obj,"L1"));
	assert(coeff->starting_point>0.0);
	coeff->starting=FIXED_SP;
	log_info("Using a fixed starting point at %f.",coeff->starting_point);
    }
    else if (strcmp(json_obj_read_string(coeff->obj,"starting"),"uniform")==0)
    {
	coeff->starting=UNIFORM_SP;
	log_info("Using uniform starting point.");
    }
    else
    {
	log_error("The starting point not well defined.");
	exit(ERROR_IMPROPER_STARTING_POINT_CONFIGURATION);
    }
    if(!json_obj_read_string(coeff->obj,"method"))
    {
	log_error("No method name given!");
	exit(ERROR_MISSING_METHOD);
    }
    if (json_obj_string_equal(coeff->obj,"method","hmyla"))
    {
	log_info("Using method: hmyla\n");
	coeff->method=&diff_r_hmyla;
	coeff->method_outlayer=&outlayer_step_uniform;
	coeff->method_right_boundary=&onestep_right_absorbing_boundary_layer_uniform_linear;
    }
    else if (json_obj_string_equal(coeff->obj,"method","uffink"))
    {
	log_info("Using method: uffink\n");
	coeff->method=&diff_r_uffink;
	coeff->method_outlayer=&outlayer_step_uniform;
	coeff->method_right_boundary=&onestep_right_absorbing_boundary_layer_uniform_linear;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbm"))
    {
	log_info("Using method: sbm\n");
	coeff->method=&diff_r_sbm_twosteps;
	coeff->method_outlayer=&outlayer_step_normal;
	coeff->method_right_boundary=&onestep_right_absorbing_boundary_layer_normal_exact;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbmlin"))
    {
	log_info("Using method: sbmlin\n");
	coeff->method=&diff_r_sbmlin;
	coeff->method_outlayer=&outlayer_step_normal;
	coeff->method_right_boundary=&onestep_right_absorbing_boundary_layer_normal_linear;
    }
    else
	{ exit_on_IMPROPER_METHOD_NAME(json_obj_read_string(coeff->obj,"method"));}
    SET_SUFFIX(coeff->suffix,coeff->obj);
    SET_PREFIX(coeff->prefix,coeff->obj);
    CREATE_DIRECTORY(coeff,filename);
    return(coeff);
}











/** @brief Simulation of the particle in the media.
 *
 * This is the main method which defines how the particle moves in the media.
 * @param[in,out] rng Random number generator.
 * @param[in] med The media.
 * @param[in,out] part The particle.
 * @param coeff The coefficient
 */
void onestep(api_rng *rng, struct_media *med, particle *part, coefficients *coeff)
{
    // Simulation close to the interfaces of the layer.
    if(onestep_disc_struct_is_in_layer(med->os1,part->xpos))
    {
	onestep_disc_struct_next(rng,med->os1,(&(part->xpos)),(&(part->time)),(coeff->method));
	if(part->xpos<=0.0)
	    { part->xpos=-part->xpos; } // reflection 
	else if (part->xpos>=med->length)
	    { part->xpos=2*med->length-part->xpos;} // reflection 
	return;
    }
    
	// outlayer step in the left with reflection
    if(part->xpos<=med->os1->position)
	{
	 part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD0,med->time,part->xpos);
	 part->time+=med->time->time;
	 part->xpos=fabs(part->xpos); // reflection at position 0 
	 return;
	}
    // Outlayer step in the right with absorption
    if(part->xpos>=med->limit_layer_right)
    {
	coeff->method_right_boundary(rng,med,part);
	return;
    }
    part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD1,med->time,part->xpos);
    part->time+=med->time->time;
    if(part->xpos>med->length)
	{ 
	    part->xpos=med->length;
	    part->alive=0;
	}
    return;
}


/** @brief the main part.
 *
 * @param argc Number of arguments
 * @param argv Vector of arguments. 
 * Use the first argument as the name of a JSON file, otherwise returns an error */
int main(int argc, char *argv[])
{
    log_info("Benchmark %s\n",basename(__FILE__));
    // For the computation time
    stopwatch *sw=stopwatch_init();



    unsigned int i;
    FILE *file_prop,*file_time;

    // For the coefficients
    coefficients *coeff;

    // For the filenames
    json_obj *filesnames =json_obj_init();


    if(argc==1)
	{ 
	    log_error("The name of a configuration file should be passed as argument.");
	    exit(ERROR_MISSING_CONFIGURATION_FILE); 
	}
    else
	{ 
	    log_info("Reading parameters in %s",argv[1]);
	    DIE_UNLESS_FILE_EXISTS(argv[1],"r");
	    coeff=coefficients_init(argv[1]); 
	}

    log_info("%lu particles will be used.\n", coeff->nb);

    api_rng * rng=api_rng_init();

    double next_time,prev_time;
    unsigned int nleft,nright;

    double positions[coeff->nb];
	
    double final_positions[coeff->nb];
    char coefficients_filename[256],proportions_filename[256],exit_times_filename[256];

    particle *part;
    part=malloc(sizeof(particle));
    struct_media *med;
    med=struct_media_init(coeff);

    // Open the files for the proportions
    log_info("Writing in the file %s%s\n",json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    sprintf(proportions_filename,"%s/proportions_%s%s",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    FILE_OPEN_OR_DIE(file_prop,proportions_filename,"w");

    // Open the file for the exit times
    sprintf(exit_times_filename,"%s/exit_time_%s%s",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    FILE_OPEN_OR_DIE(file_time,exit_times_filename,"w");

    fprintf(stderr,"%3.0f%%",0.0);
    if(coeff->starting==FIXED_SP)
	{
	for(i=0;i<coeff->nb;i++)
	    { positions[i]=coeff->starting_point;}
	}
    if(coeff->starting==UNIFORM_SP)
	{
	for(i=0;i<coeff->nb;i++)
	    { positions[i]=api_rng_uniform(rng)*med->length;}
	}

    long int nb_alive=coeff->nb;
    long int cnt_alive=0;


    stopwatch_start(sw);
    next_time=0.0;
    prev_time=0.0;
    // THE MAIN LOOP
    do 
	{
	    prev_time=next_time;
	    next_time+=coeff->deltat;
	    nleft=nright=0;
	    cnt_alive=0;
	    fprintf(stderr,"\r%.3f\t%i%%     ",next_time,(int) (100*nb_alive/((double) coeff->nb)));fflush(stderr);
	    // iterate only on the living particles
	    for(i=0;i<nb_alive;i++)
		{
		part->xpos=positions[i];
		part->time=prev_time;
		part->alive=1;
		onestep(rng,med,part,coeff);
		// if the particle is killed, then record the time
		if(part->alive==0)
		    { fprintf(file_time,"%f\n",part->time); }
		else
		    {
			if (part->xpos<med->os1->position)
			    { nleft++; }
			else
			    { nright++;}
		    final_positions[cnt_alive++]=part->xpos;
		    }
		}
	    nb_alive=cnt_alive;
	    // Write the new positions to replace the old ones
	    for(i=0;i<nb_alive;i++)
		{ positions[i]=final_positions[i]; }
	    /** Write the proportion of particles in each compartment.*/
	    fprintf(file_prop,"%f\t%g\t%g\t\n",next_time,nleft/((double) coeff->nb),nright/((double) coeff->nb));
	} while (next_time<coeff->final_time);
    fprintf(stderr,"\r\n");fflush(stderr);

    log_info("Proportions written in %s\n",proportions_filename);
    fclose(file_prop);
    log_info("Exit times written in %s\n",exit_times_filename);
    fclose(file_time);

    stopwatch_stop(sw); // stop the stopwatch 
    stopwatch_show(sw,stdout); // show the execution time
    json_obj_add_timestamp(coeff->obj); // records the current time
    json_obj_add_double(coeff->obj,"usertime",stopwatch_usertime(sw)); // records the exectution time
    free(sw); // free the stopwatch
	
    // save the paramaters of the media
    json_obj *obj_media=struct_media_to_json(med); 
    json_obj_add_string(obj_media,"leftBC","reflecting");
    json_obj_add_string(obj_media,"rightBC","absorbing");
    json_obj_add_long_int(obj_media,"layer_factor",LAYER_FACTOR);
    json_obj_add_obj(coeff->obj,"media",obj_media);
    // save the directory
    json_obj_add_string(coeff->obj,"directory",coeff->directory);
    // save the benchmark
    json_obj_add_string(coeff->obj,"benchmark",basename(__FILE__));
    // add the git commit, comment this line if GIT is not used
    json_obj_add_commit(coeff->obj,__FILE__);

    /** Save the coefficients into a file. */
    sprintf(coefficients_filename,"%s/coefficients_%s%s.json",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    log_info("Coefficients written in %s",coefficients_filename);


    // Add the name of the file for the coefficients to the list of filenames
    json_obj_add_string(filesnames,"coefficients",coefficients_filename);

    json_obj_add_string(filesnames,"proportions",proportions_filename); 
    json_obj_add_string(filesnames,"exit times",exit_times_filename); 

    json_obj_add_obj(coeff->obj,"files",filesnames); // Add the filenames to the JSON object

    json_obj_to_file(coeff->obj,coefficients_filename); // save the coefficients to a file

    json_obj_put(coeff->obj); // Free the coefficients
    free(coeff); 
    api_rng_free(rng); // free the rng
    return(EXIT_SUCCESS);
}


/** @} */

