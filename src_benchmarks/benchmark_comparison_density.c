/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

#include "sbm.h"
#include "assert.h"
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_sf_result.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include "api_json_jansson.h" 
#include "api_rng.h" 
#include "api_config.h" 
#include <dirent.h> // for directory manipulations
#include "stopwatch.h"
#include "sbm_error_code.h"
#include "log.h"

/** @brief Particle moving in an infinite composite media with one discontinuity.
 * This benchmark test compares the difference between densities
 * after a few steps (sup-norm, L2-norm, Kolmogorov-Smirnov norm).
 *
 * @file benchmark_comparison_density.c
 *
 * @author Antoine Lejay 
 * @author Géraldine Pichot
 *
 * @date 2019-01-03
 * @version 1.0
 *
 * The parameters are passed through a JSON file as a set of key/value pairs. The file shall contain
 *  - D0 diffusivity of the left part
 *  - D1 ratio so that rho=D0/D1 diffusivity of the right part
 *  - nbsteps number of steps
 *  - deltat Time step of the algorithm
 *  - nb Number of particles
 *  - starting_point Posistion of the starting point
 *  - method The name of the method, among "uffink", "hmyla", "sbm", "sbmlin"
 *  - suffix A suffix added to the names of the files.
 *  - directory The directory for writing the results.
 *
 *  The output files (the suffix is added to the names of the files) are: 
 *  - coefficients: the input parameters
 *  - distance: the different distances (sup-norm, L2-norm,
 *  Kolmogorov-Smirnov distance) between the approximated and the true
 *  density.
 *  - histograms: densities of the particles at the final positions
 */


//
//
/** \addtogroup benchmark_comparison_density */
/** @{ */


/** @brief Number of bins of the histogram */
#define DEFAULT_NBINS 400

/** @brief To be add to the bounds for the histogram, with the hope
 * that the exact density is integrated up to one.
 */
#define DEFAULT_EXTRA_LAYER 1



/** @brief Possibilities for the starting position */
enum StartingPosition {FIXED_SP,UNIFORM_SP};


/** \brief Coefficients */
typedef struct {
    /** Parameters contained in the file and that are only used in the initialization step. */
    json_obj *obj;
    /** Number of steps */
    size_t nbsteps;
    /** Final time, computed from nbsteps */
    double final_time;
    /** Time step */
    double deltat;
    /** Number of particles */
    size_t nb;
    /** Starting point */
    double starting_point;
    /** Type of starting */
    enum StartingPosition starting;
    /** The method */
    double (*method)(api_rng *, media *, time_struct *, double );
	/** The method outside the interface layer*/
    double (*method_outlayer)(api_rng *, double, time_struct *, double ); 
    /** @brief Name of the directory of the current simulation. */
    char prefix[50];
    /** Suffix for files */
    char suffix[50];
    /** Directory */
    char directory[512];
} coefficients;





/**\brief Initialization of the parameters from a JSON file.
 *
 * \param filename Name of the file contening the parameters in JSON format.
 *
 * \warning The content should be valid, otherwise a segmentation fault error is raised.*/
coefficients *coefficients_init(char *filename)
{
    coefficients *coeff=malloc(sizeof(coefficients));
    log_info("Reading coefficients in %s\n",filename);
    coeff->obj=json_obj_init_from_file(filename);
    // read the data
    coeff->nbsteps=(size_t)json_obj_read_long_int(coeff->obj,"nbsteps");
    assert(coeff->nbsteps>0);
    coeff->deltat=json_obj_read_double(coeff->obj,"deltat");
    assert(coeff->deltat>0);
    coeff->final_time=coeff->deltat*coeff->nbsteps;
    assert(coeff->final_time>0);
    coeff->nb=(size_t)json_obj_read_long_int(coeff->obj,"nb");
    assert(coeff->nb>0);

    // read informations about the starting point.
    coeff->starting_point=json_obj_read_double(coeff->obj,"starting_point");
    log_info("Using a fixed starting point at %f\n",coeff->starting_point);

    // read the method
    if(!json_obj_is_string(coeff->obj,"method"))
    {
	exit_on_ERROR_MISSING_METHOD;
    }
    if (json_obj_string_equal(coeff->obj,"method","hmyla"))
    {
	log_info("Method: hmyla\n");
	coeff->method=&diff_r_hmyla;
	coeff->method_outlayer=&outlayer_step_uniform;
    }
    else if (json_obj_string_equal(coeff->obj,"method","hmylanormal"))
    {
	log_info("Method: hmylanormal\n");
	coeff->method=&diff_r_hmyla;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","uffink"))
    {
	log_info("Method: uffink\n");
	coeff->method=&diff_r_uffink;
	coeff->method_outlayer=&outlayer_step_uniform;
    }
    else if (json_obj_string_equal(coeff->obj,"method","uffinknormal"))
    {
	log_info("Method: uffinknormal\n");
	coeff->method=&diff_r_uffink;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbm"))
    {
	log_info("Method: sbm\n");
	coeff->method=&diff_r_sbm_twosteps;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else if (json_obj_string_equal(coeff->obj,"method","sbmlin"))
    {
	log_info("Method: sbmlin\n");
	coeff->method=&diff_r_sbmlin;
	coeff->method_outlayer=&outlayer_step_normal;
    }
    else
	{ exit_on_IMPROPER_METHOD_NAME(json_obj_read_string(coeff->obj,"method")); }
    SET_SUFFIX(coeff->suffix,coeff->obj);
    SET_PREFIX(coeff->prefix,coeff->obj);
    CREATE_DIRECTORY(coeff,filename);
    return(coeff);
}

/***********************************************************************/

/***********************************************************************/
/* HISTOGRAMS */
/***********************************************************************/


/** @brief Structure to store the densities and related informations. 
 */
typedef struct {
    /**\brief The histogram for the density */
    gsl_histogram *h;
    /**\brief The histogram for the distribution function */
    gsl_histogram *hdf;
    /**\brief The histogram for the exact values */
    gsl_histogram *hexact;
    /**\brief The histogram for the differences */
    gsl_histogram *hdiff;
    /**\brief The histogram for the exact distribution function  */
    gsl_histogram *hdf_exact;
    /**\brief The histogram for the difference of the distribution function */
    gsl_histogram *hdf_diff;
    /**\brief The number of steps at which the density is taken */
    size_t nbsteps;
    /**\brief Is the histogram locked? */
    _Bool locked;
    /** \brief Width of the bin */
    double bin_width;
    /** \brief The time at which the density is taken */
    double time;
    /** \brief Sup-norm with respect to the exact density */
    double supnorm;
    /** \brief L2-norm with respect to the exact density */
    double L2norm;
    /** \brief Kolmogorov-Smirnov distance with respect to the distribution function */
    double KSnorm;
} density;

/** @brief Constructor for the density.
 *
 * @param time The time.
 * @param nbsteps Number of steps, for recording.
 * @param min Minimum.
 * @param max Maximum.
 *
 * @return A new density.
 */
density * density_init(double time,size_t nbsteps,double min,double max)
{
    density *dens;
    assert(min<max); 
    size_t nbins=DEFAULT_NBINS;
    dens=malloc(sizeof(density));
    dens->h=gsl_histogram_alloc (nbins);
    dens->hdf=gsl_histogram_alloc (nbins);
    dens->hexact=gsl_histogram_alloc (nbins);
    dens->hdf_exact=gsl_histogram_alloc (nbins);
    dens->hdiff=gsl_histogram_alloc (nbins);
    dens->hdf_diff=gsl_histogram_alloc (nbins);
    gsl_histogram_set_ranges_uniform (dens->h, min, max);
    gsl_histogram_set_ranges_uniform (dens->hdf, min, max);
    gsl_histogram_set_ranges_uniform (dens->hexact, min, max);
    gsl_histogram_set_ranges_uniform (dens->hdf_exact, min, max);
    gsl_histogram_set_ranges_uniform (dens->hdiff, min, max);
    gsl_histogram_set_ranges_uniform (dens->hdf_diff, min, max);
    dens->bin_width=(max-min)/nbins;
    dens->nbsteps=nbsteps;
    dens->time=time;
    dens->locked=0;
    dens->supnorm=GSL_NAN;
    dens->L2norm=GSL_NAN;
    return(dens);
}

/** @brief Destructor for a density 
 * @param dens A density.
 * @return Void.
 */
void density_free(density *dens)
{
    gsl_histogram_free (dens->h);
    gsl_histogram_free (dens->hexact);
    gsl_histogram_free (dens->hdiff);
    gsl_histogram_free (dens->hdf);
    gsl_histogram_free (dens->hdf_exact);
    gsl_histogram_free (dens->hdf_diff);
    free(dens);
    return;
}

/** @brief Reallocation of density.
 *
 * @param dens The density to reallocate.
 * @param time The time at which the density is considered, for recording.
 * @param nbsteps Number of steps, for recording.
 * @param min Minimum of the range of the density.
 * @param max Maximum of the range of the density.
 * @return The density with new bins and ranges.
 */
density * density_realloc(density *dens,double time,size_t nbsteps,double min,double max)
{
    assert(min<max); 
    gsl_histogram_set_ranges_uniform (dens->h, min, max);
    gsl_histogram_set_ranges_uniform (dens->hexact, min, max);
    gsl_histogram_set_ranges_uniform (dens->hdiff, min, max);

    gsl_histogram_set_ranges_uniform (dens->hdf, min, max);
    gsl_histogram_set_ranges_uniform (dens->hdf_exact, min, max);
    gsl_histogram_set_ranges_uniform (dens->hdf_diff, min, max);

    dens->bin_width=(max-min)/((size_t) DEFAULT_NBINS);
    dens->nbsteps=nbsteps;
    dens->time=time;
    dens->locked=0;
    return(dens);
}

/** @brief Add a data to the histogram
 * @param dens A density.
 * @param value The value to add.
 * @return Void.
 *
 * @warning The histogram shall not be locked.
 */
void density_add(density *dens, double value)
{
    assert(!dens->locked); 
    gsl_histogram_increment(dens->h,value); // add the value.
    return;
}

/** @brief Reset a density.  
 *
 * All the values are set to 0, yet the range is not changed.
 * @param dens A density.
 * @return Void.
 */

void density_reset(density *dens)
{
    dens->locked=0;
    dens->time=0.0;
    dens->nbsteps=0;
    dens->supnorm=GSL_NAN;
    dens->L2norm=GSL_NAN;
    gsl_histogram_reset(dens->h);
    gsl_histogram_reset(dens->hexact);
    gsl_histogram_reset(dens->hdiff);
    gsl_histogram_reset(dens->hdf);
    gsl_histogram_reset(dens->hdf_exact);
    gsl_histogram_reset(dens->hdf_diff);
    return;
}


/** @brief Transform the histogram into a density and lock it.  
 * @param dens A density.
 * @return Void.
 */

void density_close(density *dens)
{
    dens->locked=1;
    gsl_histogram_scale(dens->h,1/(gsl_histogram_sum(dens->h)*dens->bin_width)); // transform the histogram to a density.
    size_t i;
    double accumul=0.0;
    // Compute the distribution function
    for(i=0;i<dens->h->n;i++)
	{ accumul=dens->hdf->bin[i]=accumul+dens->h->bin[i]*dens->bin_width; }
    return;
}

/** @brief Transforms a vector of position to a new density.
 *
 * @param time The time at which the positions are taken, for recording.
 * @param nbsteps The number of steps, for recording.
 * @param positions A vector of positions.
 *
 * @return A density.
 * */

density *positions_to_density(double time, size_t nbsteps, gsl_vector *positions)
{
    density *dens=density_init(time, nbsteps,gsl_vector_min(positions),gsl_vector_max(positions));
    size_t i;
    for(i=0;i<positions->size;i++)
	{ density_add(dens,positions->data[i]); }
    density_close(dens); // transform the histogram to a density
    return(dens);
}

/** @brief Reset a density and fill it with a vector of positions.
 * 
 * @param[in,out] dens A density.
 * @param nbsteps the number of steps.
 * @param time The time at which the density is considered, for recording.
 * @param nbsteps Number of steps, for recording.
 * @param positions A vector of positions.
 * @param shift Shift to be applied to the position.
 *
 * @return Void.
 * */
void density_populate_with_positions(density *dens, double time, size_t nbsteps, gsl_vector *positions, double shift)
{
    density_realloc(dens, time, nbsteps, gsl_vector_min(positions)+shift- DEFAULT_EXTRA_LAYER, gsl_vector_max(positions)+shift + DEFAULT_EXTRA_LAYER);
    size_t i;
    for(i=0;i<positions->size;i++)
	{ density_add(dens,positions->data[i]+shift); }
    density_close(dens); // transform the histogram to a density
    return;
}


/** \brief Transform the density as a JSON object.
 *
 * @param dens The density to transform into a JSON object.
 *
 * @return a JSON object
 * */
json_obj *density_to_json(density *dens)
{
    json_obj *obj=json_obj_init();  // create the new object 
    json_obj_add_long_int(obj,"nbsteps",dens->nbsteps);

    size_t i;
    json_obj *range=json_arr_init();
    json_obj *midpoint=json_arr_init();
    json_obj *bin=json_arr_init();
    json_obj *bindf=json_arr_init();
    json_obj *binexact=json_arr_init();
    json_obj *bindiff=json_arr_init();
    json_obj *bindfexact=json_arr_init();
    json_obj *bindfdiff=json_arr_init();
    for(i=0;i<dens->h->n;i++) // there are n+1 limits for the bins
	{
	    json_arr_push_double(range,dens->h->range[i]);
	    json_arr_push_double(midpoint,0.5*(dens->h->range[i]+dens->h->range[i+1]));
	    json_arr_push_double(bin,dens->h->bin[i]);
	    json_arr_push_double(binexact,dens->hexact->bin[i]);
	    json_arr_push_double(bindiff,dens->hdiff->bin[i]);
	    json_arr_push_double(bindf,dens->hdf->bin[i]);
	    json_arr_push_double(bindfdiff,dens->hdf_diff->bin[i]);
	    json_arr_push_double(bindfexact,dens->hdf_exact->bin[i]);
	}
    json_arr_push_double(range,dens->h->range[dens->h->n]);
    json_obj_add_double(obj,"supnorm",dens->supnorm);
    json_obj_add_double(obj,"L2norm",dens->L2norm);
    json_obj_add_obj(obj,"range",range);
    json_obj_add_obj(obj,"midpoint",midpoint);
    json_obj_add_obj(obj,"pdf",bin);
    json_obj_add_obj(obj,"df",bindf);
    json_obj_add_obj(obj,"pdf.exact",binexact);
    json_obj_add_obj(obj,"pdf.diff",bindiff);
    json_obj_add_obj(obj,"df.exact",bindfexact);
    json_obj_add_obj(obj,"df.diff",bindfdiff);
    return(obj);
}

/** @brief Compute the distances between the approximate and the exact densities.
 *
 * The values \p supnorm and \p L2norm are updated.
 * 
 * @param[in,out] dens A density
 * @return Void 
 *
 * @warning The sup-norm depens on the width of the histogram, due to the Monte Carlo simulation.
 *
 * @warning The KS distance is not normalized at this stage.
 * */

void density_compute_distance(density *dens)
{

    assert(dens->locked); // check if the histogram is locked.
    dens->supnorm=GSL_MAX(fabs(gsl_histogram_max_val(dens->hdiff)),fabs(gsl_histogram_min_val(dens->hdiff)));
    dens->KSnorm=GSL_MAX(fabs(gsl_histogram_max_val(dens->hdf_diff)),fabs(gsl_histogram_min_val(dens->hdf_diff)));

    size_t i;
    dens->L2norm=0.0;
    for(i=0;i<dens->hdiff->n;i++)
	{
	    dens->L2norm+=gsl_pow_2(dens->hdiff->bin[i]); 
	}
    // normalize the L2-norm
    dens->L2norm=sqrt(dens->L2norm)*dens->bin_width;
    return;
}






/***********************************************************************/
/***********************************************************************/

/*** \brief Structure for the position (in time and space) of the particle. */
typedef struct {
    /** Position */
    double xpos;
    /** Time */
    double time;
} particle;


/** \brief Structure to store data related to one interface. */
typedef struct {
    /** \brief Structure for the media.*/
    media *media;
    /** \brief Time step and its square root.*/
    time_struct *time;
    /** \brief Position of the left-end limit of the interface layer.*/
    double left_layer;
    /** \brief Position of the right-end limit of the interface layer.*/
    double right_layer;
    /** \brief Position of the interface */
    double position;
} onestep_disc_struct;


/***********************************************************************/

/** \brief Check if the position is in the layer. */
int onestep_disc_struct_is_in_layer(onestep_disc_struct *os,double pos)
{
    return((pos>=os->left_layer) && (pos<=os->right_layer));
}


/** \brief Change the position of a process generated by a divergence form operator with a given algorithm around an interface. 
 *
 * @param[in,out] rng Random Number Generator.
 * @param[in] os Structure containing the interface.
 * @param[in,out] pos Pointer to the position, updated.
 * @param[in,out] time Point to the time, updated.
 * @param[in] ds Function to perform one step with a discontinuity at 0.
 */
void onestep_disc_struct_next(api_rng *rng,onestep_disc_struct *os,double *pos,double *time,double (*ds)(api_rng *, media *, time_struct *, double ))
{
    (*time)+=os->time->time;
    (*pos)=(*ds)(rng,os->media,os->time,(*pos)-os->position)+os->position;
    return;
}

/** \brief Initialize a onestep structure.
 * @param[in] d1 Diffusion coefficient on the left of the interface.
 * @param[in] d2 Diffusion coefficient on the right of the interface.
 * @param[in] position Position of the interface.
 * @param[in] deltat Time step.
 * @return A new structure gathering data about interfaces.
 */
onestep_disc_struct *onestep_disc_struct_init(double d1, double d2, double position, double deltat) 
{
    onestep_disc_struct *os;
    os=malloc(sizeof(onestep_disc_struct));
    os->media=media_init(d1,d2,LAYER_FACTOR);
    os->time=time_struct_init(deltat);
    os->left_layer=position-os->time->sqrt_time*LAYER_FACTOR*os->media->sqrtd1;
    os->right_layer=position+os->time->sqrt_time*LAYER_FACTOR*os->media->sqrtd2;
    os->position=position;
    return(os);
}

/** \brief Structure for defining the media and encoding its geometry. 
 *
 * @warning The diffusion coefficient which is store is twice the diffusion coefficient
 * which is defined.
 * */
typedef struct {
    /** \brief Diffusion coefficient in the left side. */
    double D0;
    /** \brief Square-roof of the diffusion coefficient in the left side. */
    double sqrtD0;
    /** \brief Diffusion coefficient in the right-side. */
    double D1;
    /** \brief Square-roof of the diffusion coefficient in the right side. */
    double sqrtD1;
    /** \brief Ratio D0/D1 */
    double rho;
    /** \brief The coefficient theta */
    double theta;
    /** \brief Time step for the particle. */
    double deltat;
    /** \brief Structure for the time step of the particle. */
    time_struct *time;
    /** \brief Structure for dealing with the interfaces. */
    onestep_disc_struct *os1;
} struct_media;


/** Tranform the media into a JSON object. */
json_obj *struct_media_to_json(struct_media *med)
{
    json_obj *obj=json_obj_init(); 
    json_obj_add_string(obj,"type","bimaterial");
    json_obj_add_double(obj,"D0",0.5*med->D0);
    json_obj_add_double(obj,"D1",0.5*med->D1);
    json_obj_add_double(obj,"rho",med->rho);
    json_obj_add_double(obj,"theta",med->theta);
    json_obj *layers=json_arr_init();
    json_arr_push_double(layers,0.0);
    json_arr_push_double(layers,med->os1->left_layer);
    json_arr_push_double(layers,med->os1->position);
    json_arr_push_double(layers,med->os1->right_layer);
    json_obj_add_obj(obj,"layers",layers);
    json_obj_add_double(obj,"interface",0.0);
    json_obj_add_string(obj,"left_BC","reflection");
    json_obj_add_string(obj,"right_BC","reflection");
    return(obj);
}


/** \warning Simulate the process generated by the operator \f$\nabla(D\nabla\cdot) \f$
 * so that the diffusion coefficients D0 and D1 are multiplied by 2.
 *
 * \param[in,out] coeff The parameters. 
 * The media characteristics are stored as JSON key/values pair in \p coeff->obj
 */
struct_media *struct_media_init(coefficients *coeff)
{
    struct_media *med;
    med=malloc(sizeof(struct_media));
    // Simulation with the one-half factor
    med->D0=2*json_obj_read_double(coeff->obj,"D0");
    assert(med->D0>0.0); 
    med->sqrtD0=sqrt(med->D0);
    med->D1=json_obj_read_double(coeff->obj,"D1");
    assert(med->D1>0.0);
    med->sqrtD1=sqrt(med->D1);
    // Compute rho from D1
    med->rho=med->D0/med->D1;
    // the coefficient theta in (-1,1).
    med->theta=(med->sqrtD1-med->sqrtD0)/(med->sqrtD1+med->sqrtD0);
    assert(med->theta>-1 && med->theta<1);
    med->deltat=coeff->deltat;
    med->time=time_struct_init(coeff->deltat);
    // interface layer around the interface at 0
    med->os1=onestep_disc_struct_init(med->D0,med->D1,0.0,coeff->deltat); 
    json_obj *obj_media=struct_media_to_json(med);
    json_obj_add_obj(coeff->obj,"media",obj_media);
    return(med);
}


/** @brief Simulation of one step for the particle moving in the media.
* @param[in,out] rng Random number generator
* @param[in] med The media
* @param[in,out] part The particle
* @param[in] coeff The coefficients
* @return Void
*/
void onestep(api_rng *rng, struct_media *med, particle *part, coefficients *coeff)
{
    // Simulation close to the interfaces of the layer.
    if(onestep_disc_struct_is_in_layer(med->os1,part->xpos))
    {
	onestep_disc_struct_next(rng,med->os1,(&(part->xpos)),(&(part->time)),(coeff->method));
	return;
    }
    // outlayer step in the left with reflection
    if(part->xpos<=med->os1->position)
	{
	part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD0,med->time,part->xpos);
	part->time+=med->time->time;
	return;
	}
    // Outlayer step in the right with reflection
    part->xpos=(*coeff->method_outlayer)(rng,med->sqrtD1,med->time,part->xpos);
    part->time+=med->time->time;
    return;
}


/***********************************************************************/

/**@brief Density of a process generated by a divergence form operator. 
 *
 * @param[in] time The time step.
 * @param[in] x position of the starting point.
 * @param[in] y position of the ending point.
 * @param[in] med The media.
 * @return A new position.
 * */

double transition(double time, double x, double y, struct_media *med)
{
    double xprime=x>0 ? x/med->sqrtD1 : x/med->sqrtD0;
    double yprime=y>0 ? y/med->sqrtD1 : y/med->sqrtD0;
    double corrector=y>0 ? 1/med->sqrtD1 : 1/med->sqrtD0;
    return(d_sbm(med->theta,time,xprime,yprime)*corrector);
}

/** Compute the exact density and its difference from the empirical one.
 *
 * The distance between the exact and the empirical ones are computed 
 * through \p density_compute_distance and stored in the member of \p dens.
 *
 * @param[in,out] dens Empirical density to be compared with the exact one. 
 * @param[in] startingpoint Position of the starting point.
 * @param[in] med The media.
 * @return Void
 */
void density_addexact(density *dens,double startingpoint,struct_media *med)
{
    size_t i;
    double midpoint;
    assert(dens->locked); // check if the density has already been computed for the difference.
    for(i=0;i<dens->hexact->n;i++)
    {
	midpoint=0.5*(dens->hexact->range[i]+dens->hexact->range[i+1]); // compute the midpoint
	dens->hexact->bin[i]=transition(dens->time,startingpoint,midpoint,med);
	dens->hdiff->bin[i]=dens->h->bin[i]-dens->hexact->bin[i];
    }
    double integral=gsl_histogram_sum(dens->hexact)*dens->bin_width;
    assert(1-1e-3<integral<=1+1e-3);
    // compute the distribution function
    double accumul=0.0;
    // Compute the cumulated sum for the distribution function
    for(i=0;i<dens->h->n;i++)
	{ 
	    accumul=dens->hdf_exact->bin[i]=accumul+dens->hexact->bin[i]*dens->bin_width; 
	    dens->hdf_diff->bin[i]=dens->hdf->bin[i]-dens->hdf_exact->bin[i];
	}

    density_compute_distance(dens); // compute the distance between the approximate and the exact densities.
    return;
}

/***********************************************************************/



/** @brief the main part.
 *
 * @param argc Number of arguments
 * @param argv Vector of arguments. 
 *
 * Use the first argument as the name of a JSON file. */
int main(int argc, char *argv[])
{

    log_info("Benchmark %s\n",basename(__FILE__));

    // For the time of computations.
    stopwatch *sw=stopwatch_init();

    // For the filenames
    json_obj *filesnames =json_obj_init();

    // Initialization of the generator.
    api_rng * rng=api_rng_init();

    size_t i,steps;

    // Reading the coefficients stored in a JSON file.
    coefficients *coeff;
    READ_CONFIGURATION_FILE(coeff);

    // For printing informations
    double ratio = 100.0/((double) coeff->nb);

    // Density
    density * dens=density_init(0.0,0,0,1);

    // For storing the positions
    gsl_vector *positions=gsl_vector_alloc(coeff->nb);

    char coefficients_filename[512];
    char histogram_filename[512];
    char distance_filename[512]; // for the distances
    FILE *distance_file; // connection for the distances' file. 

    /** Save the coefficients into a file. */
    sprintf(distance_filename,"%s/distances_%s%s.dat",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    log_info("Distances written in %s",distance_filename);
    FILE_OPEN_OR_DIE(distance_file,distance_filename,"w"); // check if the file could be open
    fprintf(distance_file,"no\ttime\tmax\tL2\tKS\n"); // header line.
    fclose(distance_file); // this file will be reponed later

    particle *part;

    // The media
    part=malloc(sizeof(particle));
    struct_media *med;
    med=struct_media_init(coeff);

    fprintf(stderr,"%3.0f%%",0.0);

    // Initialize the starting point for all the particles.
    // For a fixed starting point.
    for(i=0;i<coeff->nb;i++)
	{ gsl_vector_set(positions,i,coeff->starting_point);}


  // THE MAIN LOOP
  // The computations are done in series: a loop is performed for each time step and each 
  // particle.
    stopwatch_start(sw); // The execution time does not count allocation
    part->time=0.0;
    for(steps=0;steps<coeff->nbsteps;steps++)
    {
	for(i=0;i<coeff->nb;i++)
	    {
	    fprintf(stderr,"\b\b\b\b%3.0f%%",i*ratio); // print the number of particles.
	    part->xpos=gsl_vector_get(positions,i);
	    // advance each particle step per steps
	    onestep(rng,med,part,coeff); 
	    part->time-=coeff->deltat; // the time should be substracted
	    // set up the final position (this is not used) and shift it by the interface position point
	    gsl_vector_set(positions,i,part->xpos);
	    }
	part->time+=coeff->deltat; // increment the time.
	// compute the histograms.
	density_populate_with_positions(dens,part->time,steps+1,positions,0.0); // write the positions to the histogram.
	density_addexact(dens,coeff->starting_point,med);
	distance_file=fopen(distance_filename,"a"); // open the file
	// The KS distance is normalized by sqrt(# particles)
	fprintf(distance_file,"%zu\t%f\t%g\t%g\t%g\n",dens->nbsteps,dens->time,dens->supnorm,dens->L2norm,dens->KSnorm*sqrt(coeff->nb));
	fclose(distance_file);
    }
    fprintf(stderr,"\b\b\b\b\n");
    // END OF THE MAIN LOOP

    stopwatch_stop(sw); // stop the stopwatch 
    stopwatch_show(sw,stdout); // show the execution time
    json_obj_add_timestamp(coeff->obj); // records the current time
    json_obj_add_double(coeff->obj,"usertime",stopwatch_usertime(sw)); // records the exectution time
    free(sw); // free the stopwatch

    /** Save the histogram into a file with the last positions. */
    json_obj *density_obj;
    density_obj=density_to_json(dens);
    sprintf(histogram_filename,"%s/histogram_%s%s.json",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    log_info("Hіstogram written in %s\n",histogram_filename);
    json_obj_to_file(density_obj,histogram_filename);
    json_obj_put(density_obj); // Free the coefficients
    density_free(dens);


    // Adding informations to the coefficients file
    json_obj_add_long_int(coeff->obj,"layer_factor",LAYER_FACTOR);
    json_obj_add_string(coeff->obj,"directory",coeff->directory);
    json_obj_add_double(coeff->obj,"finaltime",coeff->final_time);
    json_obj_add_string(coeff->obj,"benchmark",basename(__FILE__));
    // add the git commit, comment this line if GIT is not used
    json_obj_add_commit(coeff->obj,__FILE__);

    //

    /** Save the coefficients into a file. */
    sprintf(coefficients_filename,"%s/coefficients_%s%s.json",coeff->directory,json_obj_read_string(coeff->obj,"method"),coeff->suffix);
    log_info("Coefficients written in %s\n",coefficients_filename);


    // Add the name of the file for the coefficients to the list of filenames
    json_obj_add_string(filesnames,"coefficients",coefficients_filename);
    json_obj_add_string(filesnames,"distances",distance_filename);
    json_obj_add_string(filesnames,"histograms",histogram_filename);

    json_obj_add_obj(coeff->obj,"files",filesnames); // Add the filenames to the JSON object

    json_obj_to_file(coeff->obj,coefficients_filename);

    json_obj_put(coeff->obj); // Free the coefficients
    free(coeff);

    /** free the positions */
    gsl_vector_free(positions);
    api_rng_free(rng);

    return(0);
}


/** @} */

