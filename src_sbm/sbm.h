/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

/** 
 * @file sbm.h
 * @brief Simulation of the Skew Brownian motion.
 * @author Antoine Lejay
 * @author Géraldine Pichot
 * @date 2019-01-03
 * @version 1.0
 */

#ifndef SBM_H
#define SBM_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_exp.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_sf_trig.h>
#include <gsl/gsl_sf_result.h> // for avoiding underflows and overflows
#include <gsl/gsl_errno.h> // to avoid underflow
#include <errno.h>
#include "api_rng.h"

#ifdef NCHECKINITIALIZE
#define assert_initialize(EX,MSG,CODE)
#else
/**\brief Custom assert for detecting a non-verified constrainted. 
 * The program is then interrupted with an error code.
 * @param EX the expression to check (e.g. \p a>0)
 * @param MSG the message to print
 * @param CODE error code.
 */
#define assert_initialize(EX,MSG,CODE) if(!(EX)) {fprintf(stderr,"%s:%i Initialization error (" #EX "): " MSG  "\nAborting :-(\n",__FILE__,__LINE__);exit(CODE);};
#endif

/** \addtogroup diffusion */
/** @{ */

/** \struct media
 *
 * \brief Structure to store two diffusion coefficients and the related constants. 
 *
 * The function \p media_init serves as a constructor for this structure whose
 * elements shall not be changed directly. In case of changes, \p media_reinit
 * shall be used to keep the consistency of the values. 
 *
 * This structure to stores the values related to one interface
 * between two media with distinct diffusion coefficients. 
 *
 * It should be encapsulated in a \p onestep_dfo, which also contains
 * information relative to the paths.
 *
 * The values are defined in order to compute again and again the same quantities. 
 */
typedef struct {
    /** \brief Left diffusion coefficient. */
    double d1;
    /** \brief Right diffusion coefficient. */
    double d2;
    /** \brief Square-root of the left diffusion coefficient. */
    double sqrtd1;
    /** \brief Square-root of the right diffusion coefficient. */
    double sqrtd2;
    /** @brief Skewness parameter.
     *
     * The skewness parameter is the one of the Skew Brownian motion obtained
     * after a Lamperti type transform to reduce the diffusivity to \f$1/2\f$
     * on each side. It is computed through 
     * \f$ \theta=\frac{\sqrt{d_1}-\sqrt{d_2}}{\sqrt{d_1}+\sqrt{d_2}}\in(-1,1). \f$
     *
     * The parameter \p skew belongs to \f$(-1,1)\f$. 
     * */
    double skew;
    /** @brief Renormalized skewness parameter.
     *
     * This parameter is  \f$(1+\theta)/2\in(0,1)\f$, which is related to the probability
     * that the process is positive at any given time \f$t\f$.
     *
     * \p alpha belongs to \f$(0,1)\f$. 
     **/
    double skew01;
    /** \brief Half-width of the <em>interface layer</em>.
     *
     * The interface layer is uded to delimit the zone in which a special algorithm
     * shall be used to deal with the discontiuity. Away from the interface laeyer, 
     * in a small time step, the probability of crossing the interface could
     * be neglected as this is an event whose probability decreases exponentially
     * fast with the distance from the boundary.
     *
     * This concept is defined in  \cite lejay-pichot11 and \cite lejay-pichot16.
     *
     * Usually, takes the value 4. */
    double layer;
    /** \brief Normalized length of the left layer. 
     *
     * The normalized length is without the square root of the time step. 
     * It is given by \f$\ell=\mathrm{layer}\times\sqrt{d_1}\f$.*/
    double norm_left_layer_length;
    /** \brief Normalized position of the right layer.
     *
     * The normalized length is without the square root of the time step. 
     * It is given by \f$\ell=\mathrm{layer}\times\sqrt{d_2}\f$.*/
    double norm_right_layer_length;
} media;

/** @} */


/** \addtogroup sbm */
/** @{ */

/** \def SBM_SIGN
 *\brief Returns the sign of x with the convention that 
 \code SBM_SIGN(0.0) \endcode is 1.
 */
#define SBM_SIGN(x) ((x) >=0.0 ? 1 : -1)

/** \def SBM_ASIGN
 *\brief Returns the opposite sign of x with the convention that 
 \code SBM_SIGN(0.0) \endcode is -1.
 */
#define SBM_ASIGN(x) ((x) >=0.0 ? -1 : 1)

/** @} */


/** \addtogroup time_structure */
/** @{ */
/**\struct time_struct
 * \brief Structure to contain a time step as well as its square root.
 *
 * This structure avoids to recalculate square roots of the same value a large number of times.
 *
 * @warning The value of time shall not be changed directly. Use \p time_struct_set to update.
 * */
typedef struct {
    /** \brief Time step. */
    double time; 
    /** \brief Square-root of the time step. */
    double sqrt_time; 
} time_struct;

/** @} */

/** \addtogroup diffusion_structure */
/** @{ */
/**\struct onestep_dfo
 * \brief Structure for gathering data helping to perfom one step of the algorithm close to an interface.
 *
 * The aim of this structure is to store enough information for performing one step of 
 * the simulation of a stochastic process generated by 
 * with \f$\frac{1}{2}\nabla(D\nabla\cdot)\f$ with a discontinuity at \f$x_I\f$ (attribute \p position).
 *
 * In particular, it stores the position of the left and right boundaries of the interface
 * layer, which is related to the time step.
 *
 * @warning This structure stores information related to the time step as well (which
 * determines the interface layer). Thus, any change of the time step shall lead
 * to a reinitialization of this structure.
 * */

typedef struct {
    /** \brief Structure for the media.*/
    media *media;
    /** \brief Time step and its square root.*/
    time_struct *time;
    /** \brief Position of the left-end limit of the interface layer.*/
    double left_layer;
    /** \brief Position of the right-end limit of the interface layer.*/
    double right_layer;
    /** \brief Position of the interface. */
    double position;
} onestep_dfo;
/** @} */

onestep_dfo *onestep_dfo_init(double d1, double d2, double layer, double position, double deltat);

void onestep_dfo_free(onestep_dfo *dfo);

double r_occupation_time_sbm_bridge(api_rng *rng, const double const1, const double const2);

time_struct *time_struct_init(double t);

void time_struct_set(time_struct *ts, double t);

double d_sbm(double skew, double t, double x, double y);

double r_sbm_fz(api_rng *rng, double skew, time_struct *time);

double r_sbm_fz2(api_rng *rng, double skew, time_struct *time);

double proba_pos_position(double skew, time_struct *time, double x);

double proba_neg_position(double skew, time_struct *time, double x);

double proba_crossing(double skew, time_struct *time, double x);

double r_sbm_other_side(api_rng *rng, double skew, time_struct *time, double x);

double r_sbm_same_side(api_rng *rng, double skew, time_struct *time, double x);

double r_sbm(api_rng *rng, double skew, time_struct *time, double x, int *cross);

void r_pos_bm_hit_or_continue(api_rng *rng, double horizon, double sqrt_horizon, double x, double *time, double *y,int *hit);

double r_sbm_twosteps(api_rng *rng, double skew, time_struct *time, double x, int *cross);

media * media_init(double d1, double d2, double layer);

void media_reinit(media *med, double d1, double d2, double layer);

void media_free(media *med);

/** @brief Function's pointer to a method for performing one step.
 *
 * @param[in,out] rng Random Number Generator.
 * @param[in] media The media.
 * @param[in,out] time_struct Time structure.
 * @param[in] xpos Position of the particle.
 * @return A new position.
 */

typedef double (*onestep_method)(api_rng *rng, media *med, time_struct *ts, double xpos);

void onestep_dfo_next(api_rng *rng,onestep_dfo *os,double *pos,double *time, onestep_method ds);

int onestep_dfo_is_in_layer(onestep_dfo *os,double pos);

double diff_r_sbmlin(api_rng *rng, media *med, time_struct *ts, double xpos);

double diff_r_sbm_twosteps(api_rng *rng, media *med, time_struct *ts, double xpos);

double diff_r_sbm_onestep(api_rng *rng,media *med, time_struct *ts, double xpos);

double diff_r_uffink(api_rng *rng, media *med, time_struct *ts, double xpos);

double diff_r_hmyla(api_rng *rng, media *med, time_struct *ts, double xpos);

double diff_r_nocorr_normal(api_rng *rng,media *med, time_struct *ts, double xpos);

double diff_r_nocorr_uniform(api_rng *rng,media *med, time_struct *ts, double xpos);

double outlayer_step_uniform(api_rng *rng,double sqrtd, time_struct *ts, double xpos);

double outlayer_step_normal(api_rng *rng,double sqrtd, time_struct *ts, double xpos);

#endif
