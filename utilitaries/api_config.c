/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */


/**
 * @file api_config.c
 * @brief API for configuration
 * @author Antoine Lejay
 * @author Géraldine Pichot
 * @date 2019-01-03
 * @version 1.0
 *
 * This library contains functions that are commonly called when setting the parameters.
 */
#include "api_config.h"
#include <sys/stat.h>
#include <errno.h>

#include "sbm_error_code.h"

/** \defgroup global_configuration Helpers for configuration
 *
 * \brief Helpers to define configurations. */

/** \addtogroup global_configuration */
/** @{ */



/** @brief Get the pathname.
 *
 * @param filename The name of the file.
 */
char * get_pathname(const char * filename)
{
    char *lastSlash = NULL;
    char *parent = NULL;
    char * directory = realpath(filename, NULL);
    lastSlash = strrchr(directory, '/'); 
    parent = strndup(directory , strlen(directory) - strlen(lastSlash));
    return(parent);
}


/** @brief Check if a directory may be opened
 *
 * @param dirname
 *
 * @return A boolean: 1 if the directory exists and 0 otherwise.
 * */
int directory_exists(const char *dirname)
{
    DIR *dir=opendir(dirname);
    if(dir==NULL)
        { return 0; }
    closedir(dir);
    return 1;
}

/** @brief Create a directory 
 *
 * @param dirname
 *
 * @return A boolean: 1 if the directory is created or already exists and 0 otherwise.
 * */

int create_directory(const char *dirname)
{
    int res= mkdir(dirname,0777);
    /*log_info("errno is %d\n",errno);\*/
    if(((res==-1) && errno== EEXIST)||(res==0))
    {
        return 1;
    }
    else
    {
        return 0;
    }
    
}


/** @} */
