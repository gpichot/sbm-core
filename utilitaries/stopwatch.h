/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

/** \file stopwatch.h
 * \brief Recording execution time.
 * \author Antoine Lejay
 * \author Géraldine Pichot
 */
#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <ctype.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/times.h>

/** \addtogroup stopwatch */
/* @{ */



/** @struct stopwatch 
 *
 * @brief Structure to store information related to measure the execution time.
 *
 * For the time of computations.
 * See http://pubs.opengroup.org/onlinepubs/009695399/functions/times.html
 */
typedef struct {
    /** @brief Starting time: number of clock ticks. */
     clock_t st_time; 
    /** @brief Stopping time: number of clock ticks. */
     clock_t en_time; 
    /** @brief Starting time. */
     struct tms st_cpu;
    /** @brief Stopping time. */
     struct tms en_cpu;
     /** @brief User time accumulated. */
     double accumulated_usertime; 
} stopwatch;


stopwatch * stopwatch_init();

double stopwatch_usertime(stopwatch *sw);

void stopwatch_start(stopwatch *sw);

void stopwatch_stop(stopwatch *sw);

void stopwatch_stop_and_add(stopwatch *sw);

void stopwatch_show(stopwatch *sw,FILE *stream);

#endif

/* @} */
