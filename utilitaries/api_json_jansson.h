/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

/** 
 * @file api_json.h
 * @brief API for reading, manipuation and writing JSON files and objects.
 * @author Antoine Lejay
 * @author Géraldine Pichot
 * @date 2019-01-03
 * @version 1.0
 *
 * This library aims at encapsulating the json-c library with a consistent interface 
 * so that it may be replaced with another library for reading JSON or other serialization format (XML, YAML, ...)
 * without changing the codes for the benchmark tests.
 */

#ifndef API_JSON_H
#define API_JSON_H

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stddef.h>
#include <jansson.h>
#include "sbm_error_code.h"

/** \addtogroup api_json */
/** @{ */

/** @brief Decorator for a \p json_object. 
 */
typedef struct {
    /** @brief The \p json_object. */
    json_t *obj;
} json_obj;

json_obj *json_obj_init();

void json_obj_put(json_obj *obj);

int json_obj_to_file(const json_obj *obj, const char *filename);

json_obj *json_obj_init_from_file(const char *filename);

double json_obj_read_double(const json_obj *obj, const char *key);

double json_obj_read_double_default(const json_obj *obj, const char *key, const double default_value);

long int json_obj_read_long_int(const json_obj *obj, const char *key);

long int json_obj_read_long_int_default(const json_obj *obj, const char *key, const long int default_value);

const char *json_obj_read_string_with_default(const json_obj *obj, const char *key, const char *default_string);

const char *json_obj_read_string(const json_obj *obj, const char *key);

int json_obj_string_equal(const json_obj *obj, const char *key, const char *string_to_compare);

int json_obj_is_string(const json_obj * obj, const char *key);

void json_obj_add_double(const json_obj *obj, const char *key, const double value);

void json_obj_add_string(const json_obj *obj, const char *key, const char *string);

void json_obj_add_long_int(const json_obj *obj, const char *key, const long int value);

void json_obj_add_timestamp(json_obj *obj);

json_obj *json_arr_init();

void json_arr_push_double(json_obj *obj, const double value);

void json_arr_push_string(json_obj *obj, const char *string);

void json_obj_add_obj(const json_obj *obj, const char *key, const json_obj *obj2);

void json_obj_add_commit(json_obj *obj,const char *filename);

json_obj *json_obj_read_obj(const json_obj * obj, const char *key);


/** @} */
#endif


