/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */


/** @file api_json.c
 * @brief API for JSON reading, parsing, writing
 * @author Antoine Lejay
 * @author Géraldine Pichot
 * @date 2019-08-28
 * @version 1.0
 *
 * This library provides an interface to the Jansson library (https://jansson.readthedocs.io/)
 * This way, any other JSON library 
 * may be used without changing the various exectuable.
 *
 */
#include "api_json_jansson.h"

/** \defgroup api_json API to JSON 
 *
 * \brief Interface to a JSON library */

/** \addtogroup api_json */
/** @{ */

/** @brief Format for the timestamp.*/
#define TIMESTAMP_FORMAT "%Y:%m:%d %H:%M:%S"

/** @brief Initialzation an empty \p json_obj struct.
 *
 * @return A pointer to a \p json_obj.
 */
json_obj *json_obj_init()
{
    json_obj *obj=malloc(sizeof(json_obj));
    obj->obj = json_object();
    return obj;
}


/** @brief Initialzation a \p json_obj struct with the data from a file.
 *
 * @param[in] filename The name of a file.
 *
 * @return A pointer to a \p json_obj.
 * Exit from the program if the file is empty or the object is null.
 */
json_obj *json_obj_init_from_file(const char *filename)
{
    json_obj *obj;
    json_error_t error;
    obj=json_obj_init();
    obj->obj = json_load_file(filename,0,&error);
    if(!(obj->obj)) 
    {
	fprintf(stderr,"Error while loading the file %s: return error code %i (see Jansson's doc for more info)!\n", filename,json_error_code(&error));
	exit(1);
    }
    return obj;
}

/** @brief Write a \p json_obj to a file.
 *
 * @param[in] obj The object to serialize.
 * @param[in] filename The name of the file in which it is written.
 *
 * @return (boolean) 0 or -1 according to a failure or a success.
 */
int json_obj_to_file(const json_obj *obj, const char *filename)
{
    //convert char* to file*
    FILE* myfile;
    myfile=fopen(filename,"w");
    if(myfile==NULL)
        printf("Error with opening file.");
    int out= (json_dumpf(obj->obj,myfile,JSON_ENCODE_ANY));
    fclose(myfile);
    return out;
}

/** @brief Decrement of counter to the coefficients, possibly free it if the counter is equal to 0.
 *
 * @param[out] obj The object to decrement.
 *
 * @return Void.
 */
void json_obj_put(json_obj *obj)
{
    json_decref(obj->obj);
    return;
}

/** @brief Read a long integer from a json object with a given key and return it, or exit from the code otherwise.
 *
 * @param[in] obj The \p json_obj.
 * @param[in] key The key to use.
 *
 * @return A long integer, or exit from the code otherwise.
 */


long int json_obj_read_long_int(const json_obj *obj, const char *key)
{
    json_t* returnObj; 
    if ((returnObj=json_object_get(obj->obj, key))) // return NULL in case of problem
        { return (long int) json_integer_value(returnObj); }
    fprintf(stderr,"%s:%i Expecting a key %s with a double.\n",__FILE__,__LINE__,key);
    exit(EXIT_FAILURE);
}


/** @brief Read a long integer from a json object with a given key and return it, or use a default value.
 *
 * @param[in] obj The \p json_obj.
 * @param[in] key The key to use.
 * @param[in] default_value A default value to use
 *
 * @return A long integer, or exit from the code otherwise.
 */
long int json_obj_read_long_int_default(const json_obj *obj, const char *key, const long int default_value)
{
    json_t* returnObj; 
    if ((returnObj=json_object_get(obj->obj, key)))
        { return (long int) json_integer_value(returnObj); }
    return(default_value);
}


/** @brief Returns an object associated to a given key from a json object key, or exit from the code otherwise.
 *
 * @param[in] obj The JSON object
 * @param[in] key The key to use
 *
 * @return A double, or exit from the code otherwise.
 */

json_obj *json_obj_read_obj(const json_obj * obj, const char *key)
{
    json_t* returnObj; 
    json_obj* return_obj=json_obj_init();
    if ((returnObj=json_object_get(obj->obj, key)))
        {
	    return_obj->obj=returnObj;
	    return return_obj;
	}
    fprintf(stderr,"%s:%i Expecting a key '%s' with a JSON object.\n",__FILE__,__LINE__,key);
    exit(EXIT_FAILURE);
}

/** @brief Returns a double associated to a given key from a json object key, or exit from the code otherwise.
 *
 * @param[in] obj The JSON object
 * @param[in] key The key to use
 *
 * @return A double, or exit from the code otherwise.
 */

double json_obj_read_double(const json_obj *obj, const char *key)
{
    json_t* returnObj; 
    if ((returnObj=json_object_get(obj->obj, key)))
        { return json_number_value(returnObj); }
    fprintf(stderr,"%s:%i Expecting a key %s with a double.\n",__FILE__,__LINE__,key);
    exit(EXIT_FAILURE);
}

/** @brief Returns a double associated to a given key from a json object key, or return a default value.
 *
 * @param[in] obj The JSON object
 * @param[in] key The key to use
 * @param[in] default_value The default value.
 *
 * @return A double which is either the value of the key or the defaut value.
 */

double json_obj_read_double_default(const json_obj *obj, const char *key, const double default_value)
{
    json_t* returnObj; 
    if ((returnObj=json_object_get(obj->obj, key)))
        { return json_number_value(returnObj); }
    return(default_value);
}


/** @brief Returns a string associated to a given key from a json object key, or exit from the code otherwise.
 *
 * @param[in] obj The JSON object.
 * @param[in] key The key to use.
 *
 * @return A pointer to a string, or exit to the code with an error.
 * */
const char *json_obj_read_string(const json_obj * obj, const char *key)
{
    json_t* returnObj; 
    if ((returnObj=json_object_get(obj->obj, key)))
        { return json_string_value(returnObj); }
    fprintf(stderr,"%s:%i Expecting a key '%s' with a string.\n",__FILE__,__LINE__,key);
    exit(EXIT_FAILURE);
}

/** @brief Check if the value associated to a key is a non empty string. 
 *
 * @param[in] obj The JSON object.
 * @param[in] key The key to check.
 *
 * @return 1 is the key has a value which is a non-empty string, 0 otherwise (even if the key is not defined).
 * */
int json_obj_is_string(const json_obj * obj, const char *key)
{
    json_t* returnObj; 
    if ((returnObj=json_object_get(obj->obj,key))) // returns NULL unless a valid key is found.
        { 
	    return json_string_length(returnObj)>0;  // returns 0 if the string is not of type json_type_string
	}
    return(0);
}

/** @brief Check if a string associated to a key is equal to another one, exit from the code if no string is 
 * associated to the key.
 *
 * @param[in] obj The object.
 * @param[in] key The key whose associated value is compared.
 * @param[in] string_to_compare The string for comparision. 
 *
 * @return 1 if the two strings are equal, 0 if they are not equal, exit from the code if no string is associated to the key.
 * */
int json_obj_string_equal(const json_obj *obj, const char *key, const char *string_to_compare)
{
    return strcmp(json_obj_read_string(obj,key),string_to_compare)==0;
}

/** @brief Read a string from a json object with a given key and return it, or use a default string.
 *
 * @param [in] obj The JSON object.
 * @param [in] key The key to use.
 * @param [in] default_string Default argument to use.
 *
 * @return A pointer to a string. 
 */
const char *json_obj_read_string_with_default(const json_obj *obj, const char *key, const char *default_string)
{
    json_t* returnObj; 
    if ((returnObj=json_object_get(obj->obj, key)))
        { return(json_string_value(returnObj)); }
    return(default_string);
}

/** @brief Add a string associated to a key in an object.
 *
 * @param[out] obj The object to which a key is added. 
 * @param[in] key The key.
 * @param[in] string The associated value.
 *
 * @return Void.
 */
void json_obj_add_string(const json_obj *obj, const char *key, const char *string)
{
    json_object_set(obj->obj,key,json_string(string));
    return;
}

/** @brief Add a float associated to a key in an object.
 *
 * @param[out] obj The object to which a key is added. 
 * @param[in] key The key.
 * @param[in] value The associated value.
 *
 * @return Void.
 */
void json_obj_add_double(const json_obj *obj, const char *key, const double value)
{
    json_object_set(obj->obj,key,json_real(value));
    return;
}

/** @brief Add a long integer associated to a key in an object.
 *
 * @param[out] obj The object to which a key is added. 
 * @param[in] key The key.
 * @param[in] x The associated value.
 *
 * @return Void.
 */
void json_obj_add_long_int(const json_obj *obj, const char *key, const long int x)
{
    json_object_set(obj->obj,key,json_integer((json_int_t) x));
    return;
}


/** @brief Add an object associated to a key in an object.
 *
 * @param[out] obj The object to which a key is added. 
 * @param[in] key The key.
 * @param[in] obj2 The associated JSON object.
 *
 * @return Void.
 *
 * @warning Only the \p obj value of \p obj2 is added (here, \p obj2 is a decorator).
 */
void json_obj_add_obj(const json_obj *obj, const char *key, const json_obj *obj2)
{
    json_object_set(obj->obj,key,obj2->obj);
    return;
}


/** @brief Add a timestamp in an object, associated with the key \p timestamp.
 *
 * @param[in,out] obj JSON object, to which the key/value \p * timestamp=current time * in the stopwatch library is added.
 *
 * @return Void.
 * */
void json_obj_add_timestamp(json_obj *obj)
{
    time_t rawtime;
    struct tm *info;
    char timeformat[256];
    time( &rawtime );
    info = localtime( &rawtime );
    /* Convert to local time format and print to timeformat. */
    strftime(timeformat, 26, TIMESTAMP_FORMAT, info);
    json_object_set(obj->obj,"timestamp",json_string(timeformat));
    return;
}

/** @brief Create a new array, a particular class of a JSON object.
 *
 * @return A pointer to an array.
 */
json_obj *json_arr_init()
{
    json_obj *obj;
    obj=malloc(sizeof(json_obj));
    obj->obj = json_array();
    return obj;
}

/** @brief Push a double to an array.
 *
 * @param[in,out] obj The array to change.
 * @param[in] value Doulbe to push to the array.
 *
 * @return Void.
 */
void json_arr_push_double(json_obj *obj, const double value)
{
    json_array_append_new(obj->obj,json_real(value));
    return;
}

/** @brief Push a string to an array.
 *
 * @param[in,out] obj The array to change.
 * @param[in] string String to push to the array.
 *
 * @return Void.
 */

void json_arr_push_string(json_obj *obj, const char *string)
{
    json_array_append_new(obj->obj,json_string(string));
    return;
}

/** @brief get the last commit number from git for the file 
 * and write it as a key/value with key "commit".
 *
 * @param[in,out] obj a JSON object to which a key commit will be added.
 * @param filename A string with the name of a file.
 *
 * @return Void.
 */
void json_obj_add_commit(json_obj *obj,const char *filename)
{
    char commit[256]; // to store the commit 
    char call_git[512]; // to store the command
    sprintf(call_git,"git log -1 --pretty=format:'%%H' %s",filename); 
    FILE *fp = popen(call_git, "r");
    if(fscanf(fp, "%s", commit))
      {
      pclose(fp);
      json_obj_add_string(obj,"commit",commit);
      }
    else
      {
      pclose(fp);
      json_obj_add_string(obj,"commit","∅");
      }
    return;
}



/** @} */
