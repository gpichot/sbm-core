/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */


/** \file api_json.h
 * \brief Reading and writing JSON files.
 * \author Antoine Lejay
 * \author Géraldine Pichot
 * \date 2016-12-09
 * \version 0.1
 */

#ifndef API_CONFIG_H
#define API_CONFIG_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stddef.h>
#include <dirent.h> // for directory manipulations
#include "api_json_jansson.h"
#include <libgen.h>
#include "sbm_error_code.h"
#include "log.h"


inline char separator()
{
#ifdef _WIN32
    return '\\';
#else
    return '/';
#endif
}


/** @brief Normalized width of the interface layer. */
#define LAYER_FACTOR 4.0

/** \addtogroup global_configuration */
/** @{ */

/** @brief Structure to store data related to a global configuration. 
 *
 */
typedef struct {
    /** @brief Default filename */
    char *filename;
    /** @brief JSON config */
    json_obj *json_config;
    /** @brief Default output directory */
    char *output_directory;
    /** @brief Default directory for the configuration files */
    char *configuration_directory;
    /** @brief Default configuration filename */
    char *default_configuration;
    /** @brief Fullpath of the configuration file (constructed). */
    char *default_configuration_fullpath;

    /** @brief Layer factor (use 4 if no value is given) */
    double layer_factor;
} configuration;

int directory_exists(const char *dirname);

int create_directory(const char *dirname);

char * get_pathname(const char * filename);

/** @brief Check if a file exists and can be open with a given mode.
 *
 * Use the logger
 *
 * @param filename Name of the file.
 * @param mode Mode (e.g. r w a).
 *
 * @return A file descriptor.
 * */
#define DIE_UNLESS_FILE_EXISTS(filename, mode) {\
    if(fopen(filename,mode)==NULL) \
    {\
	log_error("The file %s cannot be opened in mode %s.\n",filename,mode);\
	exit(ERROR_ON_FILE_OPENING);\
    }\
}


/** @brief Check if a directory exists or die. 
 *
 * Use the logger
 *
 * @param dir The name of the directory [const char*]
 * */

#define DIE_UNLESS_DIRECTORY_EXISTS(dir) {\
    if(directory_exists(dir)) \
	log_info("The output directory is %s\n",dir);\
    else\
    {\
	log_error("The directory %s cannot be opened.\n",dir);\
	exit(ERROR_ON_DIR_OPENING);\
    }\
}


/** @brief Create a directory or die. 
 *
 * Use the logger
 *
 * @param dir The name of the directory [const char*]
 * */

#define DIE_UNLESS_DIRECTORY_IS_CREATED(dir) {\
    int res=create_directory(dir); \
    if(res == 1) \
	log_info("The output directory is %s\n",&dir);\
    else\
    {\
    log_info("Res is %d\n",res);\
	log_error("The directory %s cannot be created.\n",dir);\
	exit(ERROR_ON_DIR_CREATION);\
    }\
}

/** @brief Read the filename and pass it through coefficients_init 
 *
 * Use the logger.
 *
 * @param coeff The coefficient 
 * **/

#define READ_CONFIGURATION_FILE(coeff) {\
    if(argc==1)\
	{ \
	  log_error("The name of a configuration file should be passed as argument.");\
          exit(ERROR_MISSING_CONFIGURATION_FILE); \
        }\
    else\
	{ \
          log_info("Reading parameters in %s",argv[1]);\
          DIE_UNLESS_FILE_EXISTS(argv[1],"r");\
          coeff=coefficients_init(argv[1]); \
      }\
}

/** @brief Open a file or die
 *
 * @param[out] file Pointer to the file (shall be defined before by FILE * file)
 * @param[in] filename
 * @param[in] mode (r, w, ...)
 *
 */
#define FILE_OPEN_OR_DIE(file,filename,mode) {\
  if((file=fopen(filename,mode))==NULL) \
    {\
	log_error("The file %s cannot be opened in mode %s.\n",filename,mode);\
	exit(ERROR_ON_FILE_OPENING);\
    }\
}

/** @brief Define the directory
 * 
 * @param coeff Coefficient structure. SHALL have keys directories [char *] and obj [json_obj].
 * @param filename The name of the filename.
 *
 **/
#define SET_DIRECTORY(coeff,filename) {\
  sprintf(coeff->directory,"%s",json_obj_read_string_with_default(coeff->obj,"directory",get_pathname(filename))); \
  DIE_UNLESS_DIRECTORY_EXISTS(coeff->directory); \
}

/** @brief Create the output directory
 * 
 * @param coeff Coefficient structure. SHALL have keys directories [char *] and obj [json_obj].
 * @param filename The name of the filename.
 *
 **/
#define CREATE_DIRECTORY(coeff,filename) {\
   sprintf(coeff->directory,"%s",json_obj_read_string_with_default(coeff->obj,"directory",get_pathname(filename)));\
   char str[2];\
   str[0] = separator();\
   str[1]= '\0';\
   DIE_UNLESS_DIRECTORY_IS_CREATED(coeff->directory);\
   sprintf(coeff->directory,"%s",strcat(coeff->directory,str));\
   sprintf(coeff->directory,"%s",strcat(coeff->directory,coeff->prefix));\
   DIE_UNLESS_DIRECTORY_IS_CREATED(coeff->directory);\
   sprintf(coeff->directory,"%s",strcat(coeff->directory,str));\
   sprintf(coeff->directory,"%s",strcat(coeff->directory,json_obj_read_string(coeff->obj,"method")));\
   log_info("coeff directory is %s\n",coeff->directory);\
   DIE_UNLESS_DIRECTORY_IS_CREATED(coeff->directory); \
}

/** @brief Set a suffix from a JSON object
 *
 * @param[out] suffix String to which the suffix is set
 * @param[in] obj JSON object with suffix, or set a default suffix
 *
 * @return void
 */

#define SET_SUFFIX(suffix, obj) {\
    if(!json_obj_read_string(obj,"suffix"))\
	{ exit(ERROR_MISSING_SUFFIX);}\
    else\
	{ sprintf(suffix,"_%s",json_obj_read_string(obj,"suffix")); }\
}

#define SET_PREFIX(prefix, obj) {\
    if(!json_obj_read_string(obj,"prefix"))\
	{ exit(ERROR_MISSING_PREFIX);}\
    else\
	{ sprintf(prefix,"%s",json_obj_read_string(obj,"prefix")); }\
}


/** @} */



#endif
