/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */


/** 
 * @file sbm_error_code.h
 * @brief Simulation of the Skew Brownian motion: error codes
 * @author Antoine Lejay
 * @author Géraldine Pichot
 * @date 2019-01-03 
 * @version 1.0
 */

#ifndef SBM_ERROR_CODE_H
#define SBM_ERROR_CODE_H

/** @brief Define error codes. */
enum errors_code {
    SUCCESS,
    FAILURE,
    ERROR_MISSING_METHOD,
    ERROR_UNCORRECT_METHOD_NAME,
    ERROR_IMPROPER_CONFIGURATION,
    ERROR_IMPROPER_LAYERS_CONFIGURATION,
    ERROR_IMPROPER_STARTING_POINT_CONFIGURATION,
    ERROR_ON_FILE_OPENING,
    ERROR_ON_DIR_OPENING,
    ERROR_MISSING_CONFIGURATION_FILE, 
    ERROR_MISSING_SUFFIX,
    ERROR_MISSING_PREFIX,
    ERROR_ON_DIR_CREATION
};

/** @brief Log the error when the method is missing  */

#define exit_on_ERROR_MISSING_METHOD {\
  log_error("No method name given");\
  exit(ERROR_MISSING_METHOD);\
}

/** @brief Log the error when the method is missing  */

#define exit_on_IMPROPER_METHOD_NAME(name) {\
  fprintf(stderr,"Not a correct name %s",name);\
  exit(ERROR_UNCORRECT_METHOD_NAME);\
}


/** @brief Log the error when the method is missing  */

#define exit_on_IMPROPER_STARTING_POINT_CONFIGURATION {\
  fprintf(stderr,"Improper starting point configuration");\
  exit(ERROR_IMPROPER_STARTING_POINT_CONFIGURATION);\
}

#endif
