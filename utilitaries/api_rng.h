/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */


/** \file api_rng.h
 * \brief API for random number generators
 * \author Antoine Lejay
 * \author Géraldine Pichot
 * \date 2019-01-03
 * \version 1.0
 */

#ifndef API_RNG_H
#define API_RNG_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_exp.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_sf_trig.h>
#include <gsl/gsl_sf_result.h> // for avoiding underflows and overflows
#include <gsl/gsl_errno.h> // to avoid underflow
#include <errno.h>

/** @brief Structure that decorate a random number generator.
 */
typedef struct {
    /** @brief The random number generator.*/
    gsl_rng *rng;
    /** @brief The type of the random number generator.*/
    const gsl_rng_type * type_rng;
} api_rng;

api_rng * api_rng_init();

void api_rng_free(api_rng *rng);

const char* api_rng_name(const api_rng *rng);

inline double api_rng_uniform(api_rng *rng)
    { return gsl_rng_uniform(rng->rng); }

inline double api_rng_gaussian(api_rng *rng, double sd)
    { return gsl_ran_gaussian(rng->rng,sd); }

inline double api_rng_ugaussian(api_rng *rng)
    { return gsl_ran_gaussian(rng->rng,1.0); }

inline double api_rng_half_ugaussian(api_rng *rng)
    { return fabs(gsl_ran_gaussian(rng->rng,1.0)); }

inline double api_rng_exponential(api_rng *rng,const double mean)
    { return gsl_ran_exponential(rng->rng,mean); }

inline double api_rng_ugaussian_tail(api_rng *rng,const double threshold)
    { return gsl_ran_ugaussian_tail (rng->rng, threshold); }

double api_rng_ugaussian_tail_negative(api_rng *rng, double threshold);

double api_rng_arcsine(api_rng *rng);

double api_rng_inverse_gaussian(api_rng *rng, double mu, double lambda);

double api_rng_unsigned_gaussian_ziggurat(api_rng * r);

double api_rng_gaussian_ziggurat(api_rng * r);
#endif
