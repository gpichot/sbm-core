/* 
 * =========================================================================
 * This file is part of the SBM software. 
 * SBM software contains proprietary and confidential information of Inria. 
 * All rights reserved. 
 * Version V1.0, July 2017
 * Authors: Antoine Lejay, Géraldine Pichot
 * Copyright (C) 2017, Inria 
 * =========================================================================
 */

/** 
 * @file stopwatch.c
 * @brief Recording execution time.
 * @author Antoine Lejay
 * @author Géraldine Pichot
 *
 * @date 2019-01-03
 * @version 1.0
 */
#ifndef SBM_STOPWATCH_H
#define SBM_STOPWATCH_H

#include "stopwatch.h"

/***********************************************************************/

// For the time of computations.
// See http://pubs.opengroup.org/onlinepubs/009695399/functions/times.html
//
/** \addtogroup stopwatch */
/* @{ */


/** @brief Constructor for a stopwatch */
stopwatch * stopwatch_init()
{
  stopwatch *sw=malloc(sizeof(stopwatch));
  sw->accumulated_usertime=0.0;
  return sw;
}

/** @brief Returns the user time.
 *
 * @param sw A stopwatch
 * @return A user time in seconds
 * */
double stopwatch_usertime(stopwatch *sw)
{
    intmax_t CPS=sysconf(_SC_CLK_TCK);
    return((double)((intmax_t)(sw->en_cpu.tms_utime - sw->st_cpu.tms_utime))/CPS);
}

/** @brief Start the stopwatch.
 *
 * @param sw A stopwatch.
 * @return Void.
 */

void stopwatch_start(stopwatch *sw)
{
  sw->st_time = times(&(sw->st_cpu)); // Start the stopwatch, st_cpu is updated
  return;
}

/** @brief Stop the stopwatch and store the result in accumulated user time (whose value is then erased).
 *
 * @param sw A stopwatch
 */
void stopwatch_stop(stopwatch *sw)
{
  sw->en_time = times(&(sw->en_cpu)); // Stop the stopwatch, en_cpu is updated
  sw->accumulated_usertime=stopwatch_usertime(sw); // store only the last time laps
  return;
}

/** @brief Stop the stopwatch and add the result to accumulated user time (whose value is then erased).
 *
 * @param sw A stopwatch
 * @return Void.
 */
void stopwatch_stop_and_add(stopwatch *sw)
{
  sw->en_time = times(&(sw->en_cpu)); // Stop the stopwatch, en_cpu is updated
  sw->accumulated_usertime+=stopwatch_usertime(sw);
  return;
}

/** @brief Show the elasped time: total, user and system time. 
 *
 * @param sw A stopwatch.
 * @param stream A stream.
 * @return Void.
 * @warning the accumulated time is not shown.
 */
void stopwatch_show(stopwatch *sw,FILE *stream)
{
    intmax_t CPS=sysconf(_SC_CLK_TCK);
    fprintf(stream,"Real Time: %.2f, User Time %.2f, System Time %.2f\n",
        (double)((intmax_t)(sw->en_time - sw->st_time))/CPS,
        (double)((intmax_t)(sw->en_cpu.tms_utime - sw->st_cpu.tms_utime))/CPS,
        (double)((intmax_t)(sw->en_cpu.tms_stime - sw->st_cpu.tms_stime))/CPS);
    return;
}

#endif

/* @} */

